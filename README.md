# Dados Abertos - Informações de Combustíveis

### Projeto desenvolvido com Stack Spring e Angular.

#### Versões das tecnologias utilizadas.

* Spring-boot: 2.1.9;
* Hibernate: 5.3.12;
* Banco H2: 1.4.199;
* Java: 1.8;
* Apache Maven: 3.6.1;
* Apache Tomcat: 9.26;
* Angular CLI: 8.2.0;
* Node: 12.7.0;
* Npm: 6.10.0.

## Como Iniciar o Projeto

* Iniciar a API com Maven; Dentro da pasta raiz do projeto, execute: **mvn spring-boot:run**
* Caso a API seja iniciada em ambiente com Sistema Operacional Linux, é necessário: **sudo**
* Iniciar o Angular Cliente; Dentro da pasta raiz do projeto,
execute: **cd fuel-app** em seguida: **ng serve**

## Documentação da API

* A documentação da API se encontra na raiz do projeto: **DOCUMENTACAO-API.yaml**
* Desenvolvida com a ferramenta: [Swagger.io](http://editor.swagger.io/)

## Dados em planilha Excel no formato CSV

* Os arquivos foram baixados do Site: [ANP.GOV.BR](http://www.anp.gov.br/dados-abertos-anp) > **Série Histórica de Preços de Combustíveis.**
* Foram configurados com formato CSV, padrão UTF-8.
* Estão disponíveis na pasta **PLANILHAS-CSV**, na raiz do projeto.