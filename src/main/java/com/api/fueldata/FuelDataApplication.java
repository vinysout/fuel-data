package com.api.fueldata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@SpringBootApplication
@Component
public class FuelDataApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(FuelDataApplication.class, args);
	}
	 
}
