package com.api.fueldata.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.api.fueldata.model.DadosCombustivel;

@Repository
public interface FuelDataRepoCustom {
	
	List<DadosCombustivel> findAll();

	List<DadosCombustivel> getByRegiao(String regiao);

	List<DadosCombustivel> getByDistrito();

	List<DadosCombustivel> getByColeta();

	List<DadosCombustivel> getAvgCompraVendaMunicipio();

	List<DadosCombustivel> getAvgCompraVendaBandeira();

}
