package com.api.fueldata.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.fueldata.model.DadosCombustivel;

@Repository
public interface FuelDataRepository extends JpaRepository<DadosCombustivel, Long>{		
	
	@Query("SELECT AVG(c.venda) FROM DadosCombustivel c WHERE c.municipio = :#{#name}")
	List<String> getMediaPreco(@Param("name") String name);
	
	
}
