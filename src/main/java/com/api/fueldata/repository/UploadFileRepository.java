package com.api.fueldata.repository;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.api.fueldata.model.UploadFileResponse;

public interface UploadFileRepository {
	
	UploadFileResponse uploadFile(MultipartFile file) throws Exception;

	ResponseEntity<Resource> downloadFile(String fileName, HttpServletRequest request);

}
