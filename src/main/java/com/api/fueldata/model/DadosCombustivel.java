package com.api.fueldata.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author souto
 *
 */

@Entity
@Table(name = "dadoscombustiveis")
public class DadosCombustivel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String regiao;
	private String estado;
	private String municipio;
	private String revenda;	
	private String cnpj;
	private String produto;
	
	@Temporal(TemporalType.DATE)
	private Date coleta;
    private Double venda;	
	private Double compra;
	private String medida;
	private String bandeira;
	
// Pois é Verboso neh?! Infelizmente o Lombok não quis me ajudar...
	
	public Long getId() {
		return id;
	}
	public String getRegiao() {
		return regiao;
	}
	public void setRegiao(String regiao) {
		this.regiao = regiao;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getRevenda() {
		return revenda;
	}
	public void setRevenda(String revenda) {
		this.revenda = revenda;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getProduto() {
		return produto;
	}
	public void setProduto(String produto) {
		this.produto = produto;
	}
	public Date getColeta() {
		return coleta;
	}
	public void setColeta(String coleta) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date   date = format.parse(coleta);
		this.coleta = date;
	}
	public Double getVenda() {
		if(this.venda == null) {
			return this.venda = 0.00;
		}else {
			return venda;
		}		
	}
	public void setVenda(String venda) {
		if(venda.trim().equals("")) {
			this.venda = 0.000;
		}else{
		venda = venda.replaceAll(",", ".");
		Double valor = Double.parseDouble(venda);
		this.venda = valor;
		}
	}
	public Double getCompra() {
		if(this.compra == null) {
			return this.compra = 0.00;
		}
		else {
			return this.compra;
		}		
	}
	public void setCompra(String compra) {
		if(compra.trim().equals("")) {
			this.compra = 0.000;
		}else{
			compra = compra.replaceAll(",", ".");
			Double valor = Double.parseDouble(compra);
			this.compra = valor;
		}		
	}
	public String getMedida() {
		return medida;
	}
	public void setMedida(String medida) {
		this.medida = medida;
	}
	public String getBandeira() {
		return bandeira;
	}
	public void setBandeira(String bandeira) {
		this.bandeira = bandeira;
	}
	
	
	
}
