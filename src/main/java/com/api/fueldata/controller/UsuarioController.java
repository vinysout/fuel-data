package com.api.fueldata.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.fueldata.exception.ResourceNotFoundException;
import com.api.fueldata.model.Usuario;
import com.api.fueldata.repository.UsuarioRepository;

@RestController
@RequestMapping("/api")
public class UsuarioController {
	
	@Autowired
	private UsuarioRepository usuarioRepo;
	
	@GetMapping("/usuarios")
	public List<Usuario> getUsuarios(){
		return usuarioRepo.findAll();
	}
	@PostMapping("/usuarios")
	public Usuario novoUsuario(@Valid @RequestBody Usuario usuario) {
		return usuarioRepo.save(usuario);
	}
	@GetMapping("/usuarios/{id}")
	public Usuario getUsuarioById(@PathVariable(value = "id") Long id) {
		return usuarioRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Usuário", "id", id));
	}
	@PutMapping("/usuarios/{id}")
	public Usuario updateUsuario(@PathVariable(value = "id") Long id,
			@Valid @RequestBody Usuario usDetails) {
		Usuario usuario = usuarioRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Usuario", "id", id));
		usuario.setNome(usDetails.getNome());
		usuario.setSenha(usDetails.getSenha());
		
		Usuario updateUsuario = usuarioRepo.save(usuario);
		return updateUsuario;
	}
	@DeleteMapping("/usuarios/{id}")
	public Usuario deleteUsuario(@PathVariable(value = "id") Long id) {
		Usuario usuario = usuarioRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Usuario", "id", id));
	usuarioRepo.delete(usuario);
	return usuario;
	}

}
