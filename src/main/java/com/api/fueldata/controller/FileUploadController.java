package com.api.fueldata.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.api.fueldata.model.UploadFileResponse;
import com.api.fueldata.repository.UploadFileRepository;

@RestController
@EnableScheduling
@RequestMapping("/api")
public class FileUploadController {	
	
	@Autowired
	private UploadFileRepository upRepo;
	
	@PostMapping("/dadosabertos/upload")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file) throws Exception {
        return upRepo.uploadFile(file);
    }
	
	@GetMapping("/dadosabertos/download/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        return upRepo.downloadFile(fileName, request);
    }
}
