package com.api.fueldata.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.fueldata.exception.ResourceNotFoundException;
import com.api.fueldata.model.DadosCombustivel;
import com.api.fueldata.repository.FuelDataRepository;
import com.api.fueldata.repository.FuelDataRepoCustom;

@RestController
@RequestMapping("/api")
public class FuelDataController {
	
	@Autowired
	private FuelDataRepository fuelRepo;
	
	@Autowired
	private FuelDataRepoCustom custom;	

	@GetMapping("/dadosabertos")
    public List<DadosCombustivel> getDatas(){    	
    	return custom.findAll();
    }
        
    @PostMapping("/dadosabertos")
    public DadosCombustivel newData(@Valid @RequestBody DadosCombustivel data) throws ParseException {    	
    	return fuelRepo.save(data);
    }
	
    @GetMapping("/dadosabertos/{id}")
    public DadosCombustivel getDataById(@PathVariable(value = "id") Long id) {
    	return fuelRepo.findById(id)
    			.orElseThrow(() -> new ResourceNotFoundException("Histórico", "id", id));
    }
    
    @PutMapping("/dadosabertos/{id}")
    public DadosCombustivel updateData(@PathVariable(value = "id") Long id,
			@Valid @RequestBody DadosCombustivel dataDetails) throws ParseException {
    	DadosCombustivel data = fuelRepo.findById(id)
    			.orElseThrow(() -> new ResourceNotFoundException("Histórico", "id", id));
    	
    	Date dataAtual = dataDetails.getColeta();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String dataFormatada = dateFormat.format(dataAtual);
            	
    	data.setRegiao(dataDetails.getRegiao());
    	data.setEstado(dataDetails.getEstado());
    	data.setMunicipio(dataDetails.getMunicipio());
    	data.setRevenda(dataDetails.getRevenda());
    	data.setCnpj(dataDetails.getCnpj());
    	data.setProduto(dataDetails.getProduto());
    	data.setColeta(dataFormatada);
    	data.setVenda(String.valueOf(dataDetails.getVenda()));
    	data.setCompra(String.valueOf(dataDetails.getCompra()));
    	data.setMedida(dataDetails.getMedida());
    	data.setBandeira(dataDetails.getBandeira());
    	
    	DadosCombustivel updateData = fuelRepo.save(data);
    	return updateData;
    	
    }
    @DeleteMapping("/dadosabertos/{id}")
    public DadosCombustivel deleteData(@PathVariable(value = "id") Long id) {
    	DadosCombustivel data = fuelRepo.findById(id)
    			.orElseThrow(() -> new ResourceNotFoundException("Histórico", "id", id));
    	fuelRepo.delete(data);
    	return data;
    }
    
    @GetMapping("/dadosabertos/mediaPrecos/{municipio}")
    public List<String> getMedia(@PathVariable(value = "municipio") String municipio){
    	return fuelRepo.getMediaPreco(municipio.toUpperCase());
    }
    
    @GetMapping("/dadosabertos/regiao/{regiao}")
    public List<DadosCombustivel> getDataByRegiao(@PathVariable(value = "regiao") String regiao){    	
    	return custom.getByRegiao(regiao);
    }
    @GetMapping("/dadosabertos/distribuidora")
    public List<DadosCombustivel> getDataByDistribuidora(){
    	return custom.getByDistrito();
    }
    @GetMapping("/dadosabertos/datacoleta")
    public List<DadosCombustivel> getDataByDataColeta(){
    	return custom.getByColeta();
    }
    @GetMapping("/dadosabertos/mediaMunicipio")
    public List<DadosCombustivel> getAvgCompraVendaMunicipio(){    	
    	return custom.getAvgCompraVendaMunicipio();
    	
    }
    @GetMapping("/dadosabertos/mediaBandeira")
    public List<DadosCombustivel> getAvgCompraVendaBandeira(){
    	return custom.getAvgCompraVendaBandeira();
    }
	
}
