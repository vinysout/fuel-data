package com.api.fueldata.impl;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.api.fueldata.controller.FileUploadController;
import com.api.fueldata.model.UploadFileResponse;
import com.api.fueldata.repository.UploadFileRepository;
import com.api.fueldata.service.FileStorageService;

@Repository
public class UploadFileRepoImpl implements UploadFileRepository{
	
	@Autowired
    private JobLauncher jobLauncher;      
	
    @Autowired
    private Job job;	
	
	@Autowired
    private FileStorageService fileStorageService;
	
	private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);

	@Override
	public UploadFileResponse uploadFile(MultipartFile file) throws Exception {
        String fileName = fileStorageService.storeFile(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/dadosabertos/download/")
                .path(fileName)
                .toUriString();
        
        JobParameters params = new JobParametersBuilder()
                .addString("JobID", String.valueOf(System.currentTimeMillis()))
                .toJobParameters();
        jobLauncher.run(job, params);
		
		return new UploadFileResponse(fileName, fileDownloadUri,
        		file.getContentType(), file.getSize());
	}

	@Override
	public ResponseEntity<Resource> downloadFile(String fileName, HttpServletRequest request) {

        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Não foi possível verificar o tipo de arquivo.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }
		return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
	}

}
