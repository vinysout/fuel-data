package com.api.fueldata.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.api.fueldata.model.DadosCombustivel;
import com.api.fueldata.repository.FuelDataRepoCustom;

@Repository
public class FuelDataRepoImpl implements FuelDataRepoCustom{
	
	@PersistenceContext
	public EntityManager em;

	@Override
    @SuppressWarnings("unchecked")
	public List<DadosCombustivel> findAll() {
		return em.createQuery(
    		    "SELECT c FROM DadosCombustivel c")
    		    .setMaxResults(10000)
    		    .getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<DadosCombustivel> getByRegiao(String regiao) {
		return em.createQuery(
    		    "SELECT c FROM DadosCombustivel c WHERE c.regiao = :regiao")
    			.setParameter("regiao", regiao.toUpperCase())
    		    .setMaxResults(10000)
    		    .getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<DadosCombustivel> getByDistrito() {
		return em.createQuery(
    			"SELECT c FROM DadosCombustivel c GROUP BY c.id ORDER BY c.revenda")
    		    .setMaxResults(10000)
    		    .getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<DadosCombustivel> getByColeta() {
		return em.createQuery(
    			"SELECT c FROM DadosCombustivel c GROUP BY c.id ORDER BY c.coleta DESC")
    		    .setMaxResults(10000)
    		    .getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<DadosCombustivel> getAvgCompraVendaMunicipio() {
		return em.createQuery(
    		    "SELECT AVG(c.compra),  AVG(c.venda), c.municipio  FROM DadosCombustivel c GROUP BY c.municipio")
    		    .setMaxResults(1000)
    		    .getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<DadosCombustivel> getAvgCompraVendaBandeira() {
		return em.createQuery(
    		    "SELECT AVG(c.compra),  AVG(c.venda), c.bandeira  FROM DadosCombustivel c GROUP BY c.bandeira")
    		    .setMaxResults(1000)
    		    .getResultList();
	}
	

}
