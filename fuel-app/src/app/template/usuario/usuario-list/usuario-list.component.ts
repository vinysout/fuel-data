import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/shared/rest-crud/usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-usuario-list',
  templateUrl: './usuario-list.component.html',
  styleUrls: ['./usuario-list.component.css']
})
export class UsuarioListComponent implements OnInit {

  users: any = [];

  constructor(public restApi: UsuarioService, public router: Router) { }

  ngOnInit() {
    this.loadUsers();
  }
  loadUsers(){
    return this.restApi.getUsers().subscribe((data: {}) => {
      this.users = data;
    })
  }
  deleteUser(id){
    if(window.confirm("Você tem certeza que deseja deletar este Usuário!?")){
      this.restApi.deleteUser(id).subscribe(data => {
        this.loadUsers();
      })
    }
  }  

}
