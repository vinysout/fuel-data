import { Component, OnInit, Input } from '@angular/core';
import { UsuarioService } from 'src/app/shared/rest-crud/usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-usuario-create',
  templateUrl: './usuario-create.component.html',
  styleUrls: ['./usuario-create.component.css']
})
export class UsuarioCreateComponent implements OnInit {

  @Input() user = {
    nome: '',
    senha: ''
  }

  constructor(public restApi: UsuarioService, public router: Router) { }

  ngOnInit() {
  }
  addUser(){
    this.restApi.createUser(this.user).subscribe((data: {}) => {
      this.router.navigate(['/usuarios'])
    })
  }

}
