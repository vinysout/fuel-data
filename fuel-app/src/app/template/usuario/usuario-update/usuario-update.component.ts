import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from 'src/app/shared/rest-crud/usuario.service';

@Component({
  selector: 'app-usuario-update',
  templateUrl: './usuario-update.component.html',
  styleUrls: ['./usuario-update.component.css']
})
export class UsuarioUpdateComponent implements OnInit {

  id = this.actRoute.snapshot.params['id'];
  user: any = {};

  constructor(public restApi: UsuarioService, public actRoute: ActivatedRoute,
              public router: Router) { }

  ngOnInit() {
    this.getUser();
  }
  getUser(){
    this.restApi.getUser(this.id).subscribe((data: {}) => {
      this.user = data;
    })
  }
  updateUser(){
    if(window.confirm('Certeza que deseja alterar dados do Usuário?')){
      this.restApi.updateUser(this.id, this.user).subscribe(data => {
        this.router.navigate(['/usuarios'])
      })
    }
  }

}
