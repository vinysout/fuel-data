import { Component, OnInit } from '@angular/core';
import { FuelDataService } from 'src/app/shared/rest-crud/fuel-data.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-fuel-data-details',
  templateUrl: './fuel-data-details.component.html',
  styleUrls: ['./fuel-data-details.component.css']
})
export class FuelDataDetailsComponent implements OnInit {
  id = this.actRoute.snapshot.params['id'];
  data: any = {};

  constructor(public apiRest: FuelDataService,public actRoute: ActivatedRoute,
              public router: Router) { }

  ngOnInit() {
    this.getDataById();
  }

  getDataById(){
    this.apiRest.getFuelDataById(this.id).subscribe((data: {}) => {
      this.data = data;
    })
  }
  deleteData(id){
    if(window.confirm("Você tem certeza que deseja deletar essas informações!?")){
      this.apiRest.deleteFuelData(id).subscribe(data => {
        this.router.navigate(['/dadosabertos'])
      })
    }
  }

}
