import { Component, OnInit } from '@angular/core';
import { FuelDataService } from 'src/app/shared/rest-crud/fuel-data.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from  '@angular/forms';
import { FuelData } from 'src/app/shared/model/fuel-data';

@Component({
  selector: 'app-fuel-data-list',
  templateUrl: './fuel-data-list.component.html',
  styleUrls: ['./fuel-data-list.component.css']
})
export class FuelDataListComponent implements OnInit {

  fuels: FuelData[];
  fuelDataResult: any = {};
  form: FormGroup;
  error: string;
  fuelData = { municipio: '', regiao:'', revenda:''};
  uploadResponse = { status: '', message: '', filePath: '' };
  pageOfItems: FuelData[];

  constructor(public restApi: FuelDataService, public router: Router,
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.loadFuelData();
    this.form = this.formBuilder.group({fileCsv: ['']});
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.form.get('fileCsv').setValue(file);
    }
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('file', this.form.get('fileCsv').value);
    this.restApi.upload(formData).subscribe(
      (res) => this.uploadResponse = res,
      (err) => this.error = err
    );
    window.alert("Em alguns instantes as informações de sua planilha serão salvas no nosso Banco!");
  }

  onChangePage(pageOfItems: Array<any>) {
    this.pageOfItems = pageOfItems;
  }
  
  loadFuelData(){
    return this.restApi.getFuelData().subscribe((data: FuelData[]) => {
      this.fuels = data;
    })
  }
  deleteData(id){
    if(window.confirm("Você tem certeza que deseja deletar esta informação!?")){
      this.restApi.deleteFuelData(id).subscribe(data => {
        this.loadFuelData();
      })
    }
  }
  searchData(){
    return this.restApi.getMediaMunicipio(this.fuelData.municipio).subscribe(data => {
      this.fuelDataResult = data;
    });
  }
}
