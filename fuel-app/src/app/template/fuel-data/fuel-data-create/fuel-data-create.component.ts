import { Component, OnInit, Input } from '@angular/core';
import { FuelDataService } from 'src/app/shared/rest-crud/fuel-data.service';
import { Router } from '@angular/router';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { ptBrLocale } from 'ngx-bootstrap/locale';

defineLocale('pt-br', ptBrLocale); 

@Component({
  selector: 'app-fuel-data-create',
  templateUrl: './fuel-data-create.component.html',
  styleUrls: ['./fuel-data-create.component.css']
})
export class FuelDataCreateComponent implements OnInit {

  datePickerConfig: Partial<BsDatepickerConfig>;

  fuelData = { medida: 'R$ / litro' }
  ufs = ['AC','AL','AM','AP','BA','CE','DF','ES','GO','MA','MG','MS','MT','PA','PB','PE','PI','PR',
       'RJ','RN','RO','RR','RS','SC','SE','SP','TO'];
  produtos = ['DIESEL','DIESEL S10','ETANOL','GASOLINA'];
  
  constructor(public restApi: FuelDataService, public router: Router,
              private localeService: BsLocaleService) {
                this.localeService.use('pt-br');
                this.datePickerConfig = Object.assign({},{
                    containerClass:'theme-dark-blue',
                    showWeekNumbers: false,
                    adaptivePosition: true
                  });
              }

  ngOnInit() {
  }
  addFuelData(){
    this.restApi.createFuelData(this.fuelData).subscribe((data: {}) => {
      this.router.navigate(['/dadosabertos'])
    })
  }

}
