import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FuelDataService } from 'src/app/shared/rest-crud/fuel-data.service';
import { FuelData } from 'src/app/shared/model/fuel-data';

@Component({
  selector: 'app-by-region',
  templateUrl: './by-region.component.html',
  styleUrls: ['./by-region.component.css']
})
export class ByRegionComponent implements OnInit {
  regiao = this.actRoute.snapshot.params['id'];
  data: FuelData[];
  pageOfItems: FuelData[];

  titulo;

  fuelDataResult: any = {};
  fuelData = {
    municipio: '',
    regiao:''
  }

  constructor(public apiRest: FuelDataService,public actRoute: ActivatedRoute,
              public router: Router) { }

  ngOnInit() {
    this.getDataByRegion();
  }

  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }

  getDataByRegion(){
    this.apiRest.getDataByRegion(this.regiao).subscribe((data: FuelData[]) => {
      this.data = data;
      this.titulo = this.data[0].regiao;
    })
  }
  deleteData(id){
    if(window.confirm("Você tem certeza que deseja deletar essas informações!?")){
      this.apiRest.deleteFuelData(id).subscribe(data => {
        this.ngOnInit();
      })
    }
  }
  searchWithReload(regiao){
    this.apiRest.getDataByRegion(regiao).subscribe((data: FuelData[]) => {
      this.data = data;
    });
    
  }
  searchMediaData(){
    return this.apiRest.getMediaMunicipio(this.fuelData.municipio).subscribe(data => {
      this.fuelDataResult = data;
    });
  }
}
