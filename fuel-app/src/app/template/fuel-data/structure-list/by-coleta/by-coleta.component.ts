import { Component, OnInit } from '@angular/core';
import { FuelData } from 'src/app/shared/model/fuel-data';
import { FuelDataService } from 'src/app/shared/rest-crud/fuel-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-by-coleta',
  templateUrl: './by-coleta.component.html',
  styleUrls: ['./by-coleta.component.css']
})
export class ByColetaComponent implements OnInit {

  fuels: FuelData[];
  pageOfItems: FuelData[];

  fuelData = {
    municipio: '',
    regiao:'',
    revenda:''
  }

  fuelDataResult: any = {};

  constructor(public restApi: FuelDataService, public router: Router) { }

  ngOnInit() {
    this.loadFuelData();
  }
  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }
  loadFuelData(){
    return this.restApi.getDataByColeta().subscribe((data: FuelData[]) => {
      this.fuels = data;
    })
  }
  deleteData(id){
    if(window.confirm("Você tem certeza que deseja deletar essas informações!?")){
      this.restApi.deleteFuelData(id).subscribe(data => {
        this.loadFuelData();
      })
    }
  }
  searchData(){
    return this.restApi.getMediaMunicipio(this.fuelData.municipio).subscribe(data => {
      this.fuelDataResult = data;
    });
  }

}
