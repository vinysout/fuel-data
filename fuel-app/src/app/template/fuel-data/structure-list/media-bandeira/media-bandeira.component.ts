import { Component, OnInit } from '@angular/core';
import { FuelDataService } from 'src/app/shared/rest-crud/fuel-data.service';
import { Router } from '@angular/router';
import { FuelData } from 'src/app/shared/model/fuel-data';

@Component({
  selector: 'app-media-bandeira',
  templateUrl: './media-bandeira.component.html',
  styleUrls: ['./media-bandeira.component.css']
})
export class MediaBandeiraComponent implements OnInit {
  
  fuels: FuelData[];
  pageOfItems: FuelData[];

  fuelData = {
    municipio: '',
    regiao:'',
    revenda:''
  }

  fuelDataResult: any = {};

  constructor(public restApi: FuelDataService, public router: Router) { }

  ngOnInit() {
    this.loadFuelData();
  }

  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }

  loadFuelData(){
    return this.restApi.getMediaByBandeira().subscribe((data: FuelData[]) => {
      this.fuels = data;
    })
  }
  searchData(){
    return this.restApi.getMediaMunicipio(this.fuelData.municipio).subscribe(data => {
      this.fuelDataResult = data;
    });
  }

}
