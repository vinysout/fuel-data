import { Component, OnInit } from '@angular/core';
import { FuelData } from 'src/app/shared/model/fuel-data';
import { FuelDataService } from 'src/app/shared/rest-crud/fuel-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-media-municipio',
  templateUrl: './media-municipio.component.html',
  styleUrls: ['./media-municipio.component.css']
})
export class MediaMunicipioComponent implements OnInit {
  
  fuels: FuelData[];
  pageOfItems: FuelData[];

  fuelData = {
    municipio: '',
    regiao:'',
    revenda:''
  }

  fuelDataResult: any = {};

  constructor(public restApi: FuelDataService, public router: Router) { }

  ngOnInit() {
    this.loadFuelData();
  }

  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }

  loadFuelData(){
    return this.restApi.getMediaByMunicipios().subscribe((data: FuelData[]) => {
      this.fuels = data;
    })
  }
  searchData(){
    return this.restApi.getMediaMunicipio(this.fuelData.municipio).subscribe(data => {
      this.fuelDataResult = data;
    });
  }

}
