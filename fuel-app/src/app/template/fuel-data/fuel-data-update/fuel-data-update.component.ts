import { Component, OnInit } from '@angular/core';
import { FuelDataService } from 'src/app/shared/rest-crud/fuel-data.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-fuel-data-update',
  templateUrl: './fuel-data-update.component.html',
  styleUrls: ['./fuel-data-update.component.css']
})
export class FuelDataUpdateComponent implements OnInit {
  id = this.actRoute.snapshot.params['id'];
  fuelData: any = {};

  ufs = ['AC','AL','AM','AP','BA','CE','DF','ES','GO','MA','MG','MS','MT','PA','PB','PE','PI','PR',
  'RJ','RN','RO','RR','RS','SC','SE','SP','TO'];
  produtos = ['DIESEL','DIESEL S10','ETANOL','GASOLINA'];

  constructor(public restApi: FuelDataService, public actRoute: ActivatedRoute,
              public router: Router) { }

  ngOnInit() {
    this.getFuelData();
  }
  getFuelData(){
    this.restApi.getFuelDataById(this.id).subscribe((data: {}) => {
      this.fuelData = data;
    })
  }
  updateFuelData(){
    if(window.confirm('Certeza que deseja alterar essas Informações?')){
      this.restApi.updateFuelData(this.id, this.fuelData).subscribe(data => {
        this.router.navigate(['/dadosabertos'])
      })
    }
  }

}
