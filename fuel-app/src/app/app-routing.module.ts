import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuarioCreateComponent } from './template/usuario/usuario-create/usuario-create.component';
import { UsuarioListComponent } from './template/usuario/usuario-list/usuario-list.component';
import { UsuarioUpdateComponent } from './template/usuario/usuario-update/usuario-update.component';
import { FuelDataCreateComponent } from './template/fuel-data/fuel-data-create/fuel-data-create.component';
import { FuelDataListComponent } from './template/fuel-data/fuel-data-list/fuel-data-list.component';
import { FuelDataDetailsComponent } from './template/fuel-data/fuel-data-details/fuel-data-details.component';
import { FuelDataUpdateComponent } from './template/fuel-data/fuel-data-update/fuel-data-update.component';
import { ByRegionComponent } from './template/fuel-data/structure-list/by-region/by-region.component';
import { ByRevendaComponent } from './template/fuel-data/structure-list/by-revenda/by-revenda.component';
import { ByColetaComponent } from './template/fuel-data/structure-list/by-coleta/by-coleta.component';
import { MediaMunicipioComponent } from './template/fuel-data/structure-list/media-municipio/media-municipio.component';
import { MediaBandeiraComponent } from './template/fuel-data/structure-list/media-bandeira/media-bandeira.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'dadosabertos' },
  { path: 'novo-usuario', component: UsuarioCreateComponent },
  { path: 'usuarios', component: UsuarioListComponent},
  { path: 'usuario/:id', component: UsuarioUpdateComponent},
  { path: 'novas-info', component: FuelDataCreateComponent},
  { path: 'dadosabertos', component: FuelDataListComponent},
  { path: 'dadosabertos/:id', component: FuelDataDetailsComponent},
  { path: 'editar-info/:id', component: FuelDataUpdateComponent},
  { path: 'regiao/:id', component: ByRegionComponent},
  { path: 'pordistribuidora', component: ByRevendaComponent},
  { path: 'datacoleta', component: ByColetaComponent},
  { path: 'media-municipio', component: MediaMunicipioComponent},
  { path: 'media-bandeira', component: MediaBandeiraComponent},
  { path: '**', component: FuelDataListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
