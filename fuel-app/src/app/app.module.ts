import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { AppComponent } from './app.component';
import { UsuarioCreateComponent } from './template/usuario/usuario-create/usuario-create.component';
import { FuelDataCreateComponent } from './template/fuel-data/fuel-data-create/fuel-data-create.component';
import { UsuarioListComponent } from './template/usuario/usuario-list/usuario-list.component';
import { UsuarioUpdateComponent } from './template/usuario/usuario-update/usuario-update.component';
import { FuelDataListComponent } from './template/fuel-data/fuel-data-list/fuel-data-list.component';
import { JwPaginationComponent } from 'jw-angular-pagination';
import { FuelDataUpdateComponent } from './template/fuel-data/fuel-data-update/fuel-data-update.component';
import { FuelDataDetailsComponent } from './template/fuel-data/fuel-data-details/fuel-data-details.component';
import { ByRegionComponent } from './template/fuel-data/structure-list/by-region/by-region.component';
import { ByRevendaComponent } from './template/fuel-data/structure-list/by-revenda/by-revenda.component';
import { ByColetaComponent } from './template/fuel-data/structure-list/by-coleta/by-coleta.component';
import { MediaMunicipioComponent } from './template/fuel-data/structure-list/media-municipio/media-municipio.component';
import { MediaBandeiraComponent } from './template/fuel-data/structure-list/media-bandeira/media-bandeira.component';

@NgModule({
  declarations: [
    AppComponent,
    UsuarioCreateComponent,
    FuelDataCreateComponent,
    UsuarioListComponent,
    UsuarioUpdateComponent,
    JwPaginationComponent,
    FuelDataListComponent,
    FuelDataUpdateComponent,
    FuelDataDetailsComponent,
    ByRegionComponent,
    ByRevendaComponent,
    ByColetaComponent,
    MediaMunicipioComponent,
    MediaBandeiraComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    Ng2SmartTableModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
