import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import {HttpClient, HttpEvent, HttpErrorResponse, HttpEventType , HttpHeaders } from '@angular/common/http';
import { FuelData } from '../model/fuel-data';
import { map } from  'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class FuelDataService {

  apiURL = "http://localhost:8080";
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json;'
    })
  }

  constructor(private http: HttpClient){}

  getFuelData(): Observable<FuelData[]>{
    return this.http.get<FuelData[]>(this.apiURL + '/api/dadosabertos')
    .pipe(
      retry(1), catchError(this.handleError)
    )
  }
  getFuelDataById(id): Observable<FuelData>{
    return this.http.get<FuelData>(this.apiURL + '/api/dadosabertos/' + id)
    .pipe(
      retry(1), catchError(this.handleError)
    )
  }
  createFuelData(data): Observable<FuelData> {
    return this.http.post<FuelData>(this.apiURL + '/api/dadosabertos', JSON.stringify(data), this.httpOptions)
    .pipe(
      retry(1), catchError(this.handleError)
    )
  }
  updateFuelData(id, data): Observable<FuelData> {
    return this.http.put<FuelData>(this.apiURL + '/api/dadosabertos/' + id, JSON.stringify(data), this.httpOptions)
    .pipe(
      retry(1), catchError(this.handleError)
    )
  }
  deleteFuelData(id): Observable<FuelData> {
    return this.http.delete<FuelData>(this.apiURL + '/api/dadosabertos/' + id, this.httpOptions)
    .pipe(
      retry(1), catchError(this.handleError)
    )
  }
  getMediaMunicipio(municipio){
    return this.http.get<FuelData>(this.apiURL + '/api/dadosabertos/mediaPrecos/' + municipio)
    .pipe(
      retry(1), catchError(this.handleError)
    )
  }
  getDataByRegion(regiao){
    return this.http.get<FuelData[]>(this.apiURL + '/api/dadosabertos/regiao/' + regiao)
    .pipe(
      retry(1), catchError(this.handleError)
    )
  }
  getDataByRevenda(){
    return this.http.get<FuelData[]>(this.apiURL + '/api/dadosabertos/distribuidora')
    .pipe(
      retry(1), catchError(this.handleError)
    )
  }
  getDataByColeta(){
    return this.http.get<FuelData[]>(this.apiURL + '/api/dadosabertos/datacoleta')
    .pipe(
      retry(1), catchError(this.handleError)
    )
  }
  getMediaByMunicipios(){
    return this.http.get<FuelData[]>(this.apiURL + '/api/dadosabertos/mediaMunicipio')
    .pipe(
      retry(1), catchError(this.handleError)
    )
  }
  getMediaByBandeira(){
    return this.http.get<FuelData[]>(this.apiURL + '/api/dadosabertos/mediaBandeira')
    .pipe(
      retry(1), catchError(this.handleError)
    )
  }


  public upload(data) {
    let uploadURL = this.apiURL + '/api/dadosabertos/upload';

    return this.http.post<any>(uploadURL, data, {
      reportProgress: true,
      observe: 'events'
    }).pipe(map((event) => {

      switch (event.type) {

        case HttpEventType.UploadProgress:
          const progress = Math.round(100 * event.loaded / event.total);
          return { status: 'progress', message: progress };

        case HttpEventType.Response:
          return event.body;
        default:
          return `Unhandled event: ${event.type}`;
      }
    })
    );
  }






  
  handleError(error){
    let errorMessage = '';
    if(error.error instanceof ErrorEvent){
      errorMessage = error.error.message;      
    } else {
      errorMessage = 'Error Code: ' + error.status + '\nMessage: ' + error.message;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
