import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Usuario } from '../model/usuario';
import {HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  apiURL = "http://localhost:8080";
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json;'
    })
  }
  
  constructor(private http: HttpClient) { }
  

  getUsers(): Observable<Usuario>{
    return this.http.get<Usuario>(this.apiURL + '/api/usuarios')
    .pipe(
      retry(1), catchError(this.handleError)
    )
  }
  getUser(id): Observable<Usuario>{
    return this.http.get<Usuario>(this.apiURL + '/api/usuarios/' + id)
    .pipe(
      retry(1), catchError(this.handleError)
    )
  }
  createUser(user): Observable<Usuario> {
    return this.http.post<Usuario>(this.apiURL + '/api/usuarios', JSON.stringify(user), this.httpOptions)
    .pipe(
      retry(1), catchError(this.handleError)
    )
  }
  updateUser(id, user): Observable<Usuario> {
    return this.http.put<Usuario>(this.apiURL + '/api/usuarios/' + id, JSON.stringify(user), this.httpOptions)
    .pipe(
      retry(1), catchError(this.handleError)
    )
  }
  deleteUser(id): Observable<Usuario> {
    return this.http.delete<Usuario>(this.apiURL + '/api/usuarios/' + id, this.httpOptions)
    .pipe(
      retry(1), catchError(this.handleError)
    )
  }

  handleError(error){
    let errorMessage = '';
    if(error.error instanceof ErrorEvent){
      errorMessage = error.error.message;      
    } else {
      errorMessage = 'Error Code: ' + error.status + '\nMessage: ' + error.message;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
