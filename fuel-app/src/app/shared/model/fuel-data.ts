export class FuelData {
    id: String;
    regiao: String;
    estado: String;
    municipio: String;
    revenda: String;
    cnpj: String;
    produto: String;
    coleta: String;
    venda: String;
    compra: String;
    medida: String;
    bandeira: String;
    media: String;
}
