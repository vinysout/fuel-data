(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"page-content-wrapper\">\n  <router-outlet></router-outlet>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/template/fuel-data/fuel-data-create/fuel-data-create.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/template/fuel-data/fuel-data-create/fuel-data-create.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n        <div class=\"col-md-12\" >\n            <h3 class=\"md-3 text-center\"><a routerLink=\"/dadosabertos\">Dados Abertos</a> - Adicionar Informações</h3>\n            <form #dataForm=\"ngForm\">\n                <div [hidden]=\"regiao.valid || regiao.pristine\" class=\"alert alert-danger\">\n                    Preencher campo Região!\n                </div>\n                <div [hidden]=\"estado.valid || estado.pristine\" class=\"alert alert-danger\">\n                    Selecionar Estado!\n                </div>\n                <div [hidden]=\"municipio.valid || municipio.pristine\" class=\"alert alert-danger\">\n                    Preencher campo Município!\n                </div>\n                <div [hidden]=\"revenda.valid || revenda.pristine\" class=\"alert alert-danger\">\n                    Preencher campo Distribuidora!\n                </div>\n                <div [hidden]=\"cnpj.valid || cnpj.pristine\" class=\"alert alert-danger\">\n                    Preencher campo CNPJ apenas com números!\n                </div>\n                <div [hidden]=\"produto.valid || produto.pristine\" class=\"alert alert-danger\">\n                    Selecionar Produto!\n                </div>\n                <div [hidden]=\"coleta.valid || coleta.pristine\" class=\"alert alert-danger\">\n                    Preencher Data da Coleta!\n                </div>\n                <div [hidden]=\"venda.valid || venda.pristine\" class=\"alert alert-danger\">\n                    Preencher Valor de Venda, digitar apenas NÚMEROS!\n                </div>\n                <div [hidden]=\"compra.valid || compra.pristine\" class=\"alert alert-danger\">\n                    Preencher Valor de Compra, digitar apenas NÚMEROS!\n                </div>\n                <div [hidden]=\"bandeira.valid || bandeira.pristine\" class=\"alert alert-danger\">\n                    Preencher campo Bandeira!\n                </div>\n\n                <div class=\"form-group col-md-12\">\n                    <label for=\"regiao\">Região:</label>\n                    <input id=\"regiao\" type=\"text\" [(ngModel)]=\"fuelData.regiao\" name=\"regiao\"\n                        class=\"form-control\" placeholder=\"Região\" required #regiao=\"ngModel\">\n                </div>          \n                <div class=\"form-group col-md-12\">\n                    <label for=\"estado\">Estado:</label>\n                    <select id=\"estado\" name=\"estado\" [(ngModel)]=\"fuelData.estado\" class=\"form-control\" pattern=\"[A-Za-z]{2}\" \n                        maxlength=\"2\" required #estado=\"ngModel\">\n                        <option *ngFor=\"let estado of ufs\" [value]=\"estado\">{{ estado }}</option>\n                    </select>\n                </div>\n                <div class=\"form-group col-md-12\">\n                    <label for=\"municipio\">Município:</label>\n                    <input id=\"municipio\" type=\"text\" [(ngModel)]=\"fuelData.municipio\" name=\"municipio\"\n                        class=\"form-control\" placeholder=\"Município\" required #municipio=\"ngModel\">\n                </div>\n                <div class=\"form-group col-md-12\">\n                    <label for=\"revenda\">Distribuidora:</label>\n                    <input id=\"revenda\" type=\"text\" [(ngModel)]=\"fuelData.revenda\" name=\"revenda\"\n                        class=\"form-control\" placeholder=\"Distribuidora\" required #revenda=\"ngModel\">\n                </div>\n                <div class=\"form-group col-md-12\">\n                    <label for=\"cnpj\">CNPJ:</label>\n                    <input id=\"cnpj\" type=\"text\" [(ngModel)]=\"fuelData.cnpj\" name=\"cnpj\" pattern=\"\\d{2}\\.\\d{3}\\.\\d{3}/\\d{4}-\\d{2}\"\n                        OnKeyPress=\"formatar('99.999.999/9999-99', this)\" maxlength=\"18\"\n                        class=\"form-control\" placeholder=\"CNPJ\" required #cnpj=\"ngModel\">\n                </div>\n                <div class=\"form-group col-md-12\">\n                    <label for=\"produto\">Produto:</label>\n                    <select id=\"produto\" name=\"produto\" [(ngModel)]=\"fuelData.produto\" class=\"form-control\" \n                        required #produto=\"ngModel\">\n                        <option *ngFor=\"let produto of produtos\" [value]=\"produto\">{{ produto }}</option>\n                    </select>\n                </div>\n                <div class=\"form-group col-md-12\">\n                    <label for=\"coleta\">Data da Coleta:</label>\n                    <input id=\"coleta\" type=\"text\" [(ngModel)]=\"fuelData.coleta\" name=\"coleta\" \n                        OnKeyPress=\"formatar('99/99/9999', this)\" maxlength=\"10\"\n                        class=\"form-control\" placeholder=\"31/12/2019\" required #coleta=\"ngModel\">\n                        <!-- [bsConfig]=\"datePickerConfig\" bsDatepicker -->\n                </div>\n                <div class=\"form-group col-md-12\">\n                    <label for=\"venda\">Valor de Venda (R$):</label>\n                    <input id=\"venda\" type=\"text\" [(ngModel)]=\"fuelData.venda\" name=\"venda\" pattern=\"\\d{1}\\.\\d+\"\n                        OnKeyPress=\"formatar('9.999', this)\" maxlength=\"5\"\n                        class=\"form-control\" placeholder=\"Valor de Venda Ex.: 4.593\" required #venda=\"ngModel\">\n                </div>\n                <div class=\"form-group col-md-12\">\n                    <label for=\"compra\">Valor de Compra (R$):</label>\n                    <input id=\"compra\" type=\"text\" [(ngModel)]=\"fuelData.compra\" name=\"compra\" pattern=\"\\d{1}\\.\\d+\"\n                        OnKeyPress=\"formatar('9.999', this)\" maxlength=\"5\"\n                        class=\"form-control\" placeholder=\"Valor de Compra Ex.: 4.593\" required #compra=\"ngModel\">\n                </div>\n                <div class=\"form-group col-md-12\">\n                    <label for=\"bandeira\">Bandeira:</label>\n                    <input id=\"bandeira\" type=\"text\" [(ngModel)]=\"fuelData.bandeira\" name=\"bandeira\"\n                        class=\"form-control\" placeholder=\"Bandeira\" required #bandeira=\"ngModel\">\n                </div>\n                <div class=\"form-group col-md-5 text-center btns\">\n                   <button class=\"btn btn-success btn-lg\" (click)=\"addFuelData()\" [disabled]=\"!dataForm.form.valid\">Adicionar</button>\n                </div>\n                <div class=\"form-group col-md-5 text-center btns\">\n                    <button class=\"btn btn-warning btn-lg\" routerLink=\"/dadosabertos\" >Cancelar</button>\n                </div>\n            </form>\n        </div>\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/template/fuel-data/fuel-data-details/fuel-data-details.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/template/fuel-data/fuel-data-details/fuel-data-details.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n        <div class=\"col-md-12\">\n            <h3 class=\"md-3 text-center\"><a routerLink=\"/dadosabertos\">Dados Abertos</a> - Informações de Combustível</h3>\n            <table class=\"table bordered striped centered highlight responsive-table\">\n                <thead>\n                    <tr>\n                        <th colspan=\"2\"><h4>Detalhes</h4></th>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr>\n                        <th scope=\"col\">Id</th>\n                        <td>{{ data.id }}</td>\n                    </tr>\n                    <tr>\n                        <th scope=\"col\">Região</th>\n                        <td>{{ data.regiao }}</td>\n                    </tr>\n                    <tr>\n                        <th scope=\"col\">Estado</th>\n                        <td>{{ data.estado }}</td>\n                    </tr>\n                    <tr>\n                        <th scope=\"col\">Município</th>\n                        <td>{{ data.municipio }}</td>\n                    </tr>\n                    <tr>\n                        <th scope=\"col\">Distribuidor</th>\n                        <td>{{ data.revenda }}</td>\n                    </tr>\n                    <tr>\n                        <th scope=\"col\">CNPJ</th>\n                        <td>{{ data.cnpj }}</td>\n                    </tr>\n                    <tr>\n                        <th scope=\"col\">Produto</th>\n                        <td>{{ data.produto }}</td>\n                    </tr>\n                    <tr>\n                        <th scope=\"col\">Data da Coleta</th>\n                        <td>{{ data.coleta | date : \"dd/MM/y\"}}</td>\n                    </tr>\n                    <tr>\n                        <th scope=\"col\">Valor de Compra</th>\n                        <td>{{ data.compra }}</td>\n                    </tr>\n                    <tr>\n                        <th scope=\"col\">Valor de Venda</th>\n                        <td>{{ data.venda }}</td>\n                    </tr>\n                    \n                    <tr>\n                        <th scope=\"col\">Unidade de Medida</th>\n                        <td>{{ data.medida }}</td>\n                    </tr>\n                </tbody>\n            </table>\n            <div class=\"form-group col-md-12\">\n                <button class=\"btn btn-warning btn-sm\" routerLink=\"/editar-info/{{data.id}}\">Editar</button>\n                <button class=\"btn btn-danger btn-sm\" (click)=\"deleteData(data.id)\">Deletar</button>\n            </div>\n        </div>\n    </div>\n    "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/template/fuel-data/fuel-data-list/fuel-data-list.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/template/fuel-data/fuel-data-list/fuel-data-list.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n            <!-- main app container -->\n        <h3 class=\"mb-3 text-center\"><a routerLink=\"/dadosabertos\">Dados Abertos</a> - Informações de Combustível</h3>\n\n        <div class=\"col-md-12 text-center\">\n            <div class=\"col-md-12\">\n                <div>\n                    <div *ngIf=\"error\">\n                        {{ error.message }}\n                    </div>\n                    <div *ngIf=\"uploadResponse.status === 'error'\">\n                        {{ uploadResponse.message }}\n                    </div>\n                    <div *ngIf=\"uploadResponse.status === 'progress'\">\n                        <div role=\"progressbar\" [style.width.%]=\"uploadResponse.message\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\">\n                        {{uploadResponse.message}}%\n                        </div>\n                    </div>\n                </div>    \n                <form [formGroup]=\"form\" (ngSubmit)=\"onSubmit()\">\n                    <div class=\"form-group col-md-12 fileup\">\n                        <input type=\"file\" name=\"avatar\" (change)=\"onFileChange($event)\" required>\n                    </div>\n                    <div class=\"form-group col-md-12\">\n                        <button class=\"btn btn-primary\" type=\"submit\">Upload CSV</button>\n                    </div>\n                </form>\n            </div>\n        </div>\n\n        <div class=\"col-md-12 no-data text-center\" *ngIf=\"fuels?.length == 0\">\n            <p>Ainda não existe Informação Cadastrada.</p>\n            <div>\n                <button class=\"btn btn-primary\" routerLink=\"/novas-info\">Adicionar Nova</button>\n            </div>\n        </div>\n        <div *ngIf=\"fuels?.length !== 0\">\n            <div class=\"col-md-12\">\n                <table class=\"table bordered striped centered highlight responsive-table\">\n                    <thead>\n                        <tr>\n                            <th><button class=\"btn btn-primary\" routerLink=\"/novas-info\">Adicionar Nova</button></th>\n                        </tr>\n                        <tr>\n                            <th colspan=\"2\">\n                                <input id=\"regiao\" type=\"text\" [(ngModel)]=\"fuelData.regiao\" name=\"regiao\" class=\"form-control col-md-2\" placeholder=\"Filtrar por Região\">\n                                <button class=\"btn btn-success btn-sm text-center col-md-6\" routerLink=\"/regiao/{{fuelData.regiao}}\">Search</button>\n                            </th>\n                            <th colspan=\"2\">\n                                <input id=\"municipio\" type=\"text\" [(ngModel)]=\"fuelData.municipio\" name=\"municipio\" class=\"form-control col-md-2\" placeholder=\"Média de preço por Municipio\">\n                                <button class=\"btn btn-success btn-sm text-center col-md-6\" (click)=\"searchData()\">Search</button>\n                                R$ {{ fuelDataResult[0] | number : \"0.00\"}}\n                            </th>\n                            <th colspan=\"2\">\n                                <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/pordistribuidora\">Agrupar por Distribuidora</button>\n                            </th>\n                            <th>\n                                <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/datacoleta\">Agrupar por Data da Coleta</button>\n                            </th>\n                            <th>\n                                <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/media-municipio\">Valor médio por Municípios</button>\n                            </th>\n                            <th>\n                                <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/media-bandeira\">Valor médio por Bandeira</button>\n                            </th>\n                        </tr>\n                        <tr>\n                            <th scope=\"col\">Região</th>\n                            <th scope=\"col\">Estado</th>\n                            <th scope=\"col\">Municipio</th>\n                            <th scope=\"col\">Distribuidora</th>\n                            <th scope=\"col\">Produto</th>\n                            <th scope=\"col\">Data da Coleta</th>\n                            <th scope=\"col\">Valor de Venda</th>\n                            <th scope=\"col\">Bandeira</th>\n                        </tr>\n                    </thead>\n                    <tbody>\n                        <tr *ngFor=\"let fuel of pageOfItems\">\n                            <td>{{fuel.regiao}}</td>\n                            <td>{{fuel.estado}}</td>\n                            <td>{{fuel.municipio}}</td>\n                            <td>{{fuel.revenda}}</td>\n                            <td>{{fuel.produto}}</td>\n                            <td>{{fuel.coleta | date : \"dd/MM/y\"}}</td>\n                            <td>{{fuel.venda}}</td>\n                            <td>{{fuel.bandeira}}</td>\n                            <td class=\"last\">\n                                <button class=\"btn btn-info btn-sm\" routerLink=\"/dadosabertos/{{fuel.id}}\">Detalhes</button>\n                                <button class=\"btn btn-warning btn-sm\" routerLink=\"/editar-info/{{fuel.id}}\">Editar</button>\n                                <button class=\"btn btn-danger btn-sm\" (click)=\"deleteData(fuel.id)\">Deletar</button>\n                            </td>\n                        </tr>\n                    </tbody>\n                </table>\n                <div class=\"card-footer pb-0 pt-3 col-md-12 text-center\">\n                    <jw-pagination [items]=\"fuels\" (changePage)=\"onChangePage($event)\"></jw-pagination>\n                </div>\n            </div>\n        </div>\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/template/fuel-data/fuel-data-update/fuel-data-update.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/template/fuel-data/fuel-data-update/fuel-data-update.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n        <div class=\"col-md-12\" >\n            <h3 class=\"md-3 text-center\"><a routerLink=\"/dadosabertos\">Dados Abertos</a> -  Alterar Informações</h3>\n            <form #dataForm=\"ngForm\">\n                <div [hidden]=\"regiao.valid || regiao.pristine\" class=\"alert alert-danger\">\n                    Preencher campo Região!\n                </div>\n                <div [hidden]=\"estado.valid || estado.pristine\" class=\"alert alert-danger\">\n                    Selecionar Estado!\n                </div>\n                <div [hidden]=\"municipio.valid || municipio.pristine\" class=\"alert alert-danger\">\n                    Preencher campo Município!\n                </div>\n                <div [hidden]=\"revenda.valid || revenda.pristine\" class=\"alert alert-danger\">\n                    Preencher campo Distribuidora!\n                </div>\n                <div [hidden]=\"cnpj.valid || cnpj.pristine\" class=\"alert alert-danger\">\n                    Preencher campo CNPJ apenas com números!\n                </div>\n                <div [hidden]=\"produto.valid || produto.pristine\" class=\"alert alert-danger\">\n                    Selecionar Produto!\n                </div>\n                <div [hidden]=\"coleta.valid || coleta.pristine\" class=\"alert alert-danger\">\n                    Preencher Data da Coleta!\n                </div>\n                <div [hidden]=\"venda.valid || venda.pristine\" class=\"alert alert-danger\">\n                    Preencher Valor de Venda, digitar apenas NÚMEROS!\n                </div>\n                <div [hidden]=\"compra.valid || compra.pristine\" class=\"alert alert-danger\">\n                    Preencher Valor de Compra, digitar apenas NÚMEROS!\n                </div>\n                <div [hidden]=\"bandeira.valid || bandeira.pristine\" class=\"alert alert-danger\">\n                    Preencher campo Bandeira!\n                </div>\n\n                <div class=\"form-group col-md-12\">\n                    <label for=\"regiao\">Região:</label>\n                    <input id=\"regiao\" type=\"text\" [(ngModel)]=\"fuelData.regiao\" name=\"regiao\"\n                        class=\"form-control\" placeholder=\"Região\" required #regiao=\"ngModel\">\n                </div>          \n                <div class=\"form-group col-md-12\">\n                    <label for=\"estado\">Estado:</label>\n                    <select id=\"estado\" name=\"estado\" [(ngModel)]=\"fuelData.estado\" class=\"form-control\" pattern=\"[A-Za-z]{2}\" \n                        maxlength=\"2\" required #estado=\"ngModel\">\n                        <option *ngFor=\"let estado of ufs\" [value]=\"estado\">{{ estado }}</option>\n                    </select>\n                </div>\n                <div class=\"form-group col-md-12\">\n                    <label for=\"municipio\">Município:</label>\n                    <input id=\"municipio\" type=\"text\" [(ngModel)]=\"fuelData.municipio\" name=\"municipio\"\n                        class=\"form-control\" placeholder=\"Município\" required #municipio=\"ngModel\">\n                </div>\n                <div class=\"form-group col-md-12\">\n                    <label for=\"revenda\">Distribuidora:</label>\n                    <input id=\"revenda\" type=\"text\" [(ngModel)]=\"fuelData.revenda\" name=\"revenda\"\n                        class=\"form-control\" placeholder=\"Distribuidora\" required #revenda=\"ngModel\">\n                </div>\n                <div class=\"form-group col-md-12\">\n                    <label for=\"cnpj\">CNPJ:</label>\n                    <input id=\"cnpj\" type=\"text\" [(ngModel)]=\"fuelData.cnpj\" name=\"cnpj\" pattern=\"\\d{2}\\.\\d{3}\\.\\d{3}/\\d{4}-\\d{2}\"\n                        OnKeyPress=\"formatar('99.999.999/9999-99', this)\" maxlength=\"18\"\n                        class=\"form-control\" placeholder=\"CNPJ\" required #cnpj=\"ngModel\">\n                </div>\n                <div class=\"form-group col-md-12\">\n                    <label for=\"produto\">Produto:</label>\n                    <select id=\"produto\" name=\"produto\" [(ngModel)]=\"fuelData.produto\" class=\"form-control\" \n                        required #produto=\"ngModel\">\n                        <option *ngFor=\"let produto of produtos\" [value]=\"produto\">{{ produto }}</option>\n                    </select>\n                </div>\n                <div class=\"form-group col-md-12\">\n                    <label for=\"coleta\">Data da Coleta:</label>\n                    <input id=\"coleta\" type=\"text\" [(ngModel)]=\"fuelData.coleta\" name=\"coleta\" pattern=\"\\d{2}\\/\\d{2}\\/\\d{4}\"\n                        OnKeyPress=\"formatar('99/99/9999', this)\" maxlength=\"10\"\n                        class=\"form-control\" placeholder=\"31/12/2019\" required #coleta=\"ngModel\">\n                        <!-- [bsConfig]=\"datePickerConfig\" bsDatepicker -->\n                </div>\n                <div class=\"form-group col-md-12\">\n                    <label for=\"venda\">Valor de Venda (R$):</label>\n                    <input id=\"venda\" type=\"text\" [(ngModel)]=\"fuelData.venda\" name=\"venda\" pattern=\"\\d{1}\\.\\d+\"\n                        OnKeyPress=\"formatar('9.999', this)\" maxlength=\"5\"\n                        class=\"form-control\" placeholder=\"Valor de Venda Ex.: 4.593\" required #venda=\"ngModel\">\n                </div>\n                <div class=\"form-group col-md-12\">\n                    <label for=\"compra\">Valor de Compra (R$):</label>\n                    <input id=\"compra\" type=\"text\" [(ngModel)]=\"fuelData.compra\" name=\"compra\" pattern=\"\\d{1}\\.\\d+\"\n                        OnKeyPress=\"formatar('9.999', this)\" maxlength=\"5\"\n                        class=\"form-control\" placeholder=\"Valor de Compra Ex.: 4.593\" required #compra=\"ngModel\">\n                </div>\n                <div class=\"form-group col-md-12\">\n                    <label for=\"bandeira\">Bandeira:</label>\n                    <input id=\"bandeira\" type=\"text\" [(ngModel)]=\"fuelData.bandeira\" name=\"bandeira\"\n                        class=\"form-control\" placeholder=\"Bandeira\" required #bandeira=\"ngModel\">\n                </div>\n                <div class=\"form-group col-md-5\">\n                   <button class=\"btn btn-success btn-lg btns\" (click)=\"updateFuelData()\" [disabled]=\"!dataForm.form.valid\">Salvar</button>\n                </div>\n                <div class=\"form-group col-md-5 text-center btns\">\n                    <button class=\"btn btn-warning btn-lg\" routerLink=\"/dadosabertos\" >Cancelar</button>\n                </div>\n            </form>\n        </div>\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/template/fuel-data/structure-list/by-coleta/by-coleta.component.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/template/fuel-data/structure-list/by-coleta/by-coleta.component.html ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n        <!-- main app container -->\n    <h3 class=\"mb-3 text-center\"><a routerLink=\"/dadosabertos\">Dados Abertos</a> - Ordenado por Data da Coleta</h3>\n\n    <div class=\"col-md-12 no-data text-center\" *ngIf=\"fuels?.length == 0\">\n        <p>Ainda não existe Informação Cadastrada.</p>\n        <div>\n            <button class=\"btn btn-primary\" routerLink=\"/novas-info\">Adicionar Nova</button>\n        </div>\n    </div>\n    <div *ngIf=\"fuels?.length !== 0\">\n        <div class=\"col-md-12\">\n            <table class=\"table bordered striped centered highlight responsive-table\">\n                <thead>\n                    <tr>\n                        <th><button class=\"btn btn-primary\" routerLink=\"/novas-info\">Adicionar Nova</button></th>\n                    </tr>\n                    <tr>\n                        <th colspan=\"2\">\n                            <input id=\"regiao\" type=\"text\" [(ngModel)]=\"fuelData.regiao\" name=\"regiao\" class=\"form-control col-md-2\" placeholder=\"Filtrar por Região\">\n                            <button class=\"btn btn-success btn-sm col-md-6\" routerLink=\"/regiao/{{fuelData.regiao}}\">Search</button>\n                        </th>\n                        <th colspan=\"2\">\n                            <input id=\"municipio\" type=\"text\" [(ngModel)]=\"fuelData.municipio\" name=\"municipio\" class=\"form-control col-md-2\" placeholder=\"Média de preço por Municipio\">\n                            <button class=\"btn btn-success btn-sm col-md-6\" (click)=\"searchData()\">Search</button>\n                            R$ {{ fuelDataResult[0] | number : \"0.00\"}}\n                        </th>\n                        <th>\n                            <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/pordistribuidora\">Agrupar por Distribuidora</button>\n                        </th>\n                        <th colspan=\"2\">\n                            <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/datacoleta\">Agrupar por Data da Coleta</button>\n                        </th>\n                        <th>\n                            <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/media-municipio\">Valor médio por Municípios</button>\n                        </th>\n                        <th colspan=\"2\">\n                            <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/media-bandeira\">Valor médio por Bandeira</button>\n                        </th>\n                    </tr>\n                    <tr>\n                        <th scope=\"col\">Região</th>\n                        <th scope=\"col\">Estado</th>\n                        <th scope=\"col\">Municipio</th>\n                        <th scope=\"col\">Distribuidora</th>\n                        <th scope=\"col\">Produto</th>\n                        <th scope=\"col\">Data da Coleta</th>\n                        <th scope=\"col\">Valor de Venda</th>\n                        <th scope=\"col\">Bandeira</th>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr *ngFor=\"let fuel of pageOfItems\">\n                        <td>{{fuel.regiao}}</td>\n                        <td>{{fuel.estado}}</td>\n                        <td>{{fuel.municipio}}</td>\n                        <td>{{fuel.revenda}}</td>\n                        <td>{{fuel.produto}}</td>\n                        <td>{{fuel.coleta | date : \"dd/MM/y\"}}</td>\n                        <td>{{fuel.venda}}</td>\n                        <td>{{fuel.bandeira}}</td>\n                        <td class=\"last\">\n                            <button class=\"btn btn-info btn-sm\" routerLink=\"/dadosabertos/{{fuel.id}}\">Detalhes</button>\n                            <button class=\"btn btn-warning btn-sm\" routerLink=\"/editar-info/{{fuel.id}}\">Editar</button>\n                            <button class=\"btn btn-danger btn-sm\" (click)=\"deleteData(fuel.id)\">Deletar</button>\n                        </td>\n                    </tr>\n                </tbody>\n            </table>\n            <div class=\"card-footer pb-0 pt-3 col-md-12 text-center\">\n                <jw-pagination [items]=\"fuels\" (changePage)=\"onChangePage($event)\"></jw-pagination>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/template/fuel-data/structure-list/by-region/by-region.component.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/template/fuel-data/structure-list/by-region/by-region.component.html ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n        <!-- main app container -->\n    <h3 class=\"mb-3 text-center\"><a routerLink=\"/dadosabertos\">Dados Abertos</a> - Filtro Por Região</h3>\n\n    <div class=\"col-md-12 no-data text-center\" *ngIf=\"data?.length == 0\">\n        <p>Ainda não existe Informações dessa Região Cadastrada.</p>\n        <div>\n            <button class=\"btn btn-primary\" routerLink=\"/novas-info\">Adicionar Nova</button>\n        </div>\n    </div>\n    <div *ngIf=\"data?.length !== 0\">\n        <div class=\"col-md-12\">\n            <table class=\"table bordered striped centered highlight responsive-table\">\n                <thead>\n                    <tr>\n                        <th><button class=\"btn btn-primary\" routerLink=\"/novas-info\">Adicionar Nova</button></th>\n                    </tr>\n                    <tr>\n                        <th colspan=\"2\">\n                            <input id=\"regiao\" type=\"text\" [(ngModel)]=\"fuelData.regiao\" name=\"regiao\" class=\"form-control col-md-2\" placeholder=\"Filtrar por Região\">\n                            <button class=\"btn btn-success btn-sm col-md-6\" (click)=\"searchWithReload(fuelData.regiao)\">Search</button>\n                        </th>\n                        <th colspan=\"2\">\n                            <input id=\"municipio\" type=\"text\" [(ngModel)]=\"fuelData.municipio\" name=\"municipio\" class=\"form-control col-md-2\" placeholder=\"Média de preço por Municipio\">\n                            <button class=\"btn btn-success btn-sm col-md-6\" (click)=\"searchMediaData()\">Search</button>\n                           R$ {{ fuelDataResult[0] | number : \"0.00\"}}\n                        </th>                        \n                        <th>\n                            <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/pordistribuidora\">Agrupar por Distribuidora</button>\n                        </th>\n                        <th>\n                            <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/datacoleta\">Agrupar por Data da Coleta</button>\n                        </th>\n                        <th colspan=\"2\">\n                            <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/media-municipio\">Valor médio por Municípios</button>\n                        </th>\n                        <th colspan=\"2\">\n                            <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/media-bandeira\">Valor médio por Bandeira</button>\n                        </th>\n                    </tr>\n                    <tr>\n                    <tr>\n                        <th scope=\"col\" colspan=\"9\">REGIÃO - {{ titulo }}</th>\n                    </tr>\n                    <tr>\n                        <th scope=\"col\">Estado</th>\n                        <th scope=\"col\">Municipio</th>\n                        <th scope=\"col\">Distribuidora</th>\n                        <th scope=\"col\">Produto</th>\n                        <th scope=\"col\">Data da Coleta</th>\n                        <th scope=\"col\">Valor de Compra</th>\n                        <th scope=\"col\">Valor de Venda</th>\n                        <th scope=\"col\">Bandeira</th>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr *ngFor=\"let fuel of pageOfItems\">\n                        <td>{{fuel.estado}}</td>\n                        <td>{{fuel.municipio}}</td>\n                        <td>{{fuel.revenda}}</td>\n                        <td>{{fuel.produto}}</td>\n                        <td>{{fuel.coleta | date : \"dd/MM/y\"}}</td>\n                        <td>{{fuel.compra}}</td>\n                        <td>{{fuel.venda}}</td>\n                        <td>{{fuel.bandeira}}</td>\n                        <td class=\"last\">\n                            <button class=\"btn btn-info btn-sm\" routerLink=\"/dadosabertos/{{fuel.id}}\">Detalhes</button>\n                            <button class=\"btn btn-warning btn-sm\" routerLink=\"/editar-info/{{fuel.id}}\">Editar</button>\n                            <button class=\"btn btn-danger btn-sm\" (click)=\"deleteData(fuel.id)\">Deletar</button>\n                        </td>\n                    </tr>\n                </tbody>\n            </table>\n            <div class=\"card-footer pb-0 pt-3 col-md-12 text-center\">\n                <jw-pagination [items]=\"data\" (changePage)=\"onChangePage($event)\"></jw-pagination>\n            </div>\n        </div>\n    </div>\n</div>\n\n<!--\n\n\n<table class=\"table bordered striped centered highlight responsive-table\">\n                <thead>\n                    <tr>\n                        <th><button class=\"btn btn-primary\" routerLink=\"/novas-info\">Adicionar Nova</button></th>\n                    </tr>\n                    <tr>\n                        <th colspan=\"2\">\n                            <input id=\"municipio\" type=\"text\" [(ngModel)]=\"fuelData.municipio\" name=\"municipio\" class=\"form-control col-md-2\" placeholder=\"Média de preço por Municipio\">\n                            <button class=\"btn btn-success btn-sm text-center col-md-6\" (click)=\"searchData()\">Search</button>\n                            R$ {{ fuelDataResult[0] | number : \"0.00\"}}\n                        </th>\n                        <th colspan=\"2\">\n                            <input id=\"regiao\" type=\"text\" [(ngModel)]=\"fuelData.regiao\" name=\"regiao\" class=\"form-control col-md-2\" placeholder=\"Filtrar por Região\">\n                            <button class=\"btn btn-success btn-sm text-center col-md-6\" routerLink=\"/regiao/{{fuelData.regiao}}\">Search</button>\n                        </th>\n                    </tr>\n                    <tr>\n                        <th scope=\"col\">Estado</th>\n                        <th scope=\"col\">Municipio</th>\n                        <th scope=\"col\">Distribuidora</th>\n                        <th scope=\"col\">Produto</th>\n                        <th scope=\"col\">Data da Coleta</th>\n                        <th scope=\"col\">Valor de Compra</th>\n                        <th scope=\"col\">Valor de Venda</th>\n                        <th scope=\"col\">Bandeira</th>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr *ngFor=\"let fuel of pageOfItems\">\n                        <td>{{fuel.estado}}</td>\n                        <td>{{fuel.municipio}}</td>\n                        <td>{{fuel.revenda}}</td>\n                        <td>{{fuel.produto}}</td>\n                        <td>{{fuel.coleta | date : \"dd/MM/y\"}}</td>\n                        <td>{{fuel.compra}}</td>\n                        <td>{{fuel.venda}}</td>\n                        <td>{{fuel.bandeira}}</td>\n                        <td class=\"last\">\n                            <button class=\"btn btn-info btn-sm\" routerLink=\"/dadosabertos/{{fuel.id}}\">Detalhes</button>\n                            <button class=\"btn btn-warning btn-sm\" routerLink=\"/editar-info/{{fuel.id}}\">Editar</button>\n                            <button class=\"btn btn-danger btn-sm\" (click)=\"deleteData(fuel.id)\">Deletar</button>\n                        </td>\n                    </tr>\n                </tbody>\n            </table>\n\n\n-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/template/fuel-data/structure-list/by-revenda/by-revenda.component.html":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/template/fuel-data/structure-list/by-revenda/by-revenda.component.html ***!
  \******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n        <!-- main app container -->\n    <h3 class=\"mb-3 text-center\"><a routerLink=\"/dadosabertos\">Dados Abertos</a> - Ordenado por Distribuidora</h3>\n\n    <div class=\"col-md-12 no-data text-center\" *ngIf=\"fuels?.length == 0\">\n        <p>Ainda não existe Informação Cadastrada.</p>\n        <div>\n            <button class=\"btn btn-primary\" routerLink=\"/novas-info\">Adicionar Nova</button>\n        </div>\n    </div>\n    <div *ngIf=\"fuels?.length !== 0\">\n        <div class=\"col-md-12\">\n            <table class=\"table bordered striped centered highlight responsive-table\">\n                <thead>\n                    <tr>\n                        <th><button class=\"btn btn-primary\" routerLink=\"/novas-info\">Adicionar Nova</button></th>\n                    </tr>\n                    <tr>\n                        <th colspan=\"2\">\n                            <input id=\"regiao\" type=\"text\" [(ngModel)]=\"fuelData.regiao\" name=\"regiao\" class=\"form-control col-md-2\" placeholder=\"Filtrar por Região\">\n                            <button class=\"btn btn-success btn-sm col-md-6\" routerLink=\"/regiao/{{fuelData.regiao}}\">Search</button>\n                        </th>\n                        <th colspan=\"2\">\n                            <input id=\"municipio\" type=\"text\" [(ngModel)]=\"fuelData.municipio\" name=\"municipio\" class=\"form-control col-md-2\" placeholder=\"Média de preço por Municipio\">\n                            <button class=\"btn btn-success btn-sm col-md-6\" (click)=\"searchData()\">Search</button>\n                            R$ {{ fuelDataResult[0] | number : \"0.00\"}}\n                        </th>\n                        <th>\n                            <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/pordistribuidora\">Agrupar por Distribuidora</button>\n                        </th>\n                        <th colspan=\"2\">\n                            <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/datacoleta\">Agrupar por Data da Coleta</button>\n                        </th>\n                        <th>\n                            <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/media-municipio\">Valor médio por Municípios</button>\n                        </th>\n                        <th colspan=\"2\">\n                            <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/media-bandeira\">Valor médio por Bandeira</button>\n                        </th>\n                    </tr>\n                    <tr>\n                        <th scope=\"col\">Região</th>\n                        <th scope=\"col\">Estado</th>\n                        <th scope=\"col\">Municipio</th>\n                        <th scope=\"col\">Distribuidora</th>\n                        <th scope=\"col\">Produto</th>\n                        <th scope=\"col\">Data da Coleta</th>\n                        <th scope=\"col\">Valor de Venda</th>\n                        <th scope=\"col\">Bandeira</th>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr *ngFor=\"let fuel of pageOfItems\">\n                        <td>{{fuel.regiao}}</td>\n                        <td>{{fuel.estado}}</td>\n                        <td>{{fuel.municipio}}</td>\n                        <td>{{fuel.revenda}}</td>\n                        <td>{{fuel.produto}}</td>\n                        <td>{{fuel.coleta | date : \"dd/MM/y\"}}</td>\n                        <td>{{fuel.venda}}</td>\n                        <td>{{fuel.bandeira}}</td>\n                        <td class=\"last\">\n                            <button class=\"btn btn-info btn-sm\" routerLink=\"/dadosabertos/{{fuel.id}}\">Detalhes</button>\n                            <button class=\"btn btn-warning btn-sm\" routerLink=\"/editar-info/{{fuel.id}}\">Editar</button>\n                            <button class=\"btn btn-danger btn-sm\" (click)=\"deleteData(fuel.id)\">Deletar</button>\n                        </td>\n                    </tr>\n                </tbody>\n            </table>\n            <div class=\"card-footer pb-0 pt-3 col-md-12 text-center\">\n                <jw-pagination [items]=\"fuels\" (changePage)=\"onChangePage($event)\"></jw-pagination>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/template/fuel-data/structure-list/media-bandeira/media-bandeira.component.html":
/*!**************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/template/fuel-data/structure-list/media-bandeira/media-bandeira.component.html ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n        <!-- main app container -->\n    <h3 class=\"mb-3 text-center\"><a routerLink=\"/dadosabertos\">Dados Abertos</a> - Valor Médio de Compra e Venda por Bandeira</h3>\n\n    <div class=\"col-md-12 no-data text-center\" *ngIf=\"fuels?.length == 0\">\n        <p>Ainda não existe Informação Cadastrada.</p>\n        <div>\n            <button class=\"btn btn-primary\" routerLink=\"/novas-info\">Adicionar Nova</button>\n        </div>\n    </div>\n    <div *ngIf=\"fuels?.length !== 0\">\n        <div class=\"col-md-12\" >\n            <table class=\"table bordered striped centered highlight responsive-table\">\n                <thead>\n                    <tr>\n                        <th><button class=\"btn btn-primary\" routerLink=\"/novas-info\">Adicionar Nova</button></th>\n                    </tr>\n                    <tr>\n                        <th>\n                            <input id=\"regiao\" type=\"text\" [(ngModel)]=\"fuelData.regiao\" name=\"regiao\" class=\"form-control col-md-2\" placeholder=\"Filtrar por Região\">\n                            <button class=\"btn btn-success btn-sm col-md-6\" routerLink=\"/regiao/{{fuelData.regiao}}\">Search</button>\n                        </th>\n                        <th >\n                            <input id=\"municipio\" type=\"text\" [(ngModel)]=\"fuelData.municipio\" name=\"municipio\" class=\"form-control col-md-2\" placeholder=\"Média de preço por Municipio\">\n                            <button class=\"btn btn-success btn-sm col-md-6\" (click)=\"searchData()\">Search</button>\n                            R$ {{ fuelDataResult[0] | number : \"0.00\"}}\n                        </th>\n                        <th>\n                            <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/pordistribuidora\">Agrupar por Distribuidora</button>\n                        </th>\n                        <th>\n                            <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/datacoleta\">Agrupar por Data da Coleta</button>\n                        </th>\n                        <th>\n                            <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/media-municipio\">Valor médio por Municípios</button>\n                        </th>\n                        <th>\n                            <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/media-bandeira\">Valor médio por Bandeira</button>\n                        </th>\n                    </tr>\n                    <tr>\n                        <th scope=\"col\" colspan=\"2\">Município</th>\n                        <th scope=\"col\" colspan=\"2\">Valor de Compra</th>\n                        <th scope=\"col\" colspan=\"2\">Valor de Venda</th>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr *ngFor=\"let fuel of pageOfItems\">\n                        <td colspan=\"2\">{{ fuel[2] }}</td>\n                        <td colspan=\"2\">R$ {{ fuel[0] | number : \"0.00\"}}</td>\n                        <td colspan=\"2\">R$ {{ fuel[1] | number : \"0.00\"}}</td>\n                    </tr>\n                </tbody>\n            </table>\n            <div class=\"card-footer pb-0 pt-3 col-md-12 text-center\">\n                <jw-pagination [items]=\"fuels\" (changePage)=\"onChangePage($event)\"></jw-pagination>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/template/fuel-data/structure-list/media-municipio/media-municipio.component.html":
/*!****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/template/fuel-data/structure-list/media-municipio/media-municipio.component.html ***!
  \****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n        <!-- main app container -->\n    <h3 class=\"mb-3 text-center\"><a routerLink=\"/dadosabertos\">Dados Abertos</a> - Valor Médio de Compra e Venda por Município</h3>\n\n    <div class=\"col-md-12 no-data text-center\" *ngIf=\"fuels?.length == 0\">\n        <p>Ainda não existe Informação Cadastrada.</p>\n        <div>\n            <button class=\"btn btn-primary\" routerLink=\"/novas-info\">Adicionar Nova</button>\n        </div>\n    </div>\n    <div *ngIf=\"fuels?.length !== 0\">\n        <div class=\"col-md-12\" >\n            <table class=\"table bordered striped centered highlight responsive-table\">\n                <thead>\n                    <tr>\n                        <th><button class=\"btn btn-primary\" routerLink=\"/novas-info\">Adicionar Nova</button></th>\n                    </tr>\n                    <tr>\n                        <th>\n                            <input id=\"regiao\" type=\"text\" [(ngModel)]=\"fuelData.regiao\" name=\"regiao\" class=\"form-control col-md-2\" placeholder=\"Filtrar por Região\">\n                            <button class=\"btn btn-success btn-sm col-md-6\" routerLink=\"/regiao/{{fuelData.regiao}}\">Search</button>\n                        </th>\n                        <th >\n                            <input id=\"municipio\" type=\"text\" [(ngModel)]=\"fuelData.municipio\" name=\"municipio\" class=\"form-control col-md-2\" placeholder=\"Média de preço por Municipio\">\n                            <button class=\"btn btn-success btn-sm col-md-6\" (click)=\"searchData()\">Search</button>\n                            R$ {{ fuelDataResult[0] | number : \"0.00\"}}\n                        </th>\n                        <th>\n                            <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/pordistribuidora\">Agrupar por Distribuidora</button>\n                        </th>\n                        <th>\n                            <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/datacoleta\">Agrupar por Data da Coleta</button>\n                        </th>\n                        <th>\n                            <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/media-municipio\">Valor médio por Municípios</button>\n                        </th>\n                        <th>\n                            <button class=\"btn btn-success btn-sm col-md-12\" routerLink=\"/media-bandeira\">Valor médio por Bandeira</button>\n                        </th>\n                    </tr>\n                    <tr>\n                        <th scope=\"col\" colspan=\"2\">Município</th>\n                        <th scope=\"col\" colspan=\"2\">Valor de Compra</th>\n                        <th scope=\"col\" colspan=\"2\">Valor de Venda</th>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr *ngFor=\"let fuel of pageOfItems\">\n                        <td colspan=\"2\">{{ fuel[2] }}</td>\n                        <td colspan=\"2\">R$ {{ fuel[0] | number : \"0.00\"}}</td>\n                        <td colspan=\"2\">R$ {{ fuel[1] | number : \"0.00\"}}</td>\n                    </tr>\n                </tbody>\n            </table>\n            <div class=\"card-footer pb-0 pt-3 col-md-12 text-center\">\n                <jw-pagination [items]=\"fuels\" (changePage)=\"onChangePage($event)\"></jw-pagination>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/template/usuario/usuario-create/usuario-create.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/template/usuario/usuario-create/usuario-create.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"col-md-12\" >\n        <h3 class=\"md-3 text-center\">Adicionar Usuário</h3>\n        <form #userForm=\"ngForm\">\n            <div *ngIf=\"userName.invalid && (userName.dirty || userName.touched)\" class=\"alert alert-danger\">\n                <div *ngIf=\"userName.errors.required\">\n                    Preencher campo Nome do Usuário!\n                </div>\n                <div *ngIf=\"userName.errors.minlength\">\n                    O Nome do Usuário deve ter no mínimo 3 caracteres.\n                </div>\n            </div><div *ngIf=\"password.invalid && (userName.dirty || password.touched)\" class=\"alert alert-danger\">\n                <div *ngIf=\"password.errors.required\">\n                    Preencher campo Senha!\n                </div>\n                <div *ngIf=\"password.errors.minlength\">\n                    Sua senha deve ter no mínimo 8 caracteres.\n                </div>\n            </div>          \n            <div class=\"form-group col-md-12\">\n                <label for=\"userName\">Usuário:</label>\n                <input id=\"userName\" type=\"text\" [(ngModel)]=\"user.nome\" name=\"userName\"\n                    class=\"form-control\" placeholder=\"Usuário\" required minlength=\"3\" #userName=\"ngModel\">\n            </div>\n            <div class=\"form-group col-md-12\">\n                <label for=\"password\">Senha:</label>\n                <input id=\"password\" type=\"password\" [(ngModel)]=\"user.senha\" name=\"password\"\n                    class=\"form-control\" placeholder=\"Senha\" required minlength=\"8\" #password=\"ngModel\">\n            </div>    \n            <div class=\"form-group col-md-12\">\n               <button class=\"btn btn-success btn-lg\" (click)=\"addUser()\" [disabled]=\"!userForm.form.valid\">Adicionar</button>\n            </div>\n        </form>\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/template/usuario/usuario-list/usuario-list.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/template/usuario/usuario-list/usuario-list.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <h3 class=\"mb-3 text-center\">Lista de Usuários</h3>\n    <button class=\"btn btn-primary\" routerLink=\"/dadosabertos\">Dados Abertos</button>\n    <div class=\"no-data text-center\" *ngIf=\"users.length == 0\">\n        <p>Ainda não existe Usuário Cadastrado.</p>\n        <button class=\"btn btn-outline-primary\" routerLink=\"/novo-usuario\">Adicionar Usuário</button>\n    </div>\n    <div *ngIf=\"users.length !== 0\">\n        \n        <div class=\"col-md-12\">\n            <button class=\"btn btn-primary\" routerLink=\"/novo-usuario\">Adicionar Usuário</button>\n            <table class=\"table bordered striped centered highlight responsive-table\">\n                <thead>\n                    <tr>\n                        <th scope=\"col\">Id</th>\n                        <th scope=\"col\">Nome</th>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr *ngFor=\"let user of users\">\n                        <td>{{user.id}}</td>\n                        <td>{{user.nome}}</td>\n                        <td class=\"last\">\n                            <button class=\"btn btn-info btn-sm\" routerLink=\"/usuario/{{user.id}}\">Editar</button>\n                            <button class=\"btn btn-danger btn-sm\" (click)=\"deleteUser(user.id)\">Deletar</button>\n                        </td>\n                    </tr>\n                </tbody>\n            </table>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/template/usuario/usuario-update/usuario-update.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/template/usuario/usuario-update/usuario-update.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n        <div class=\"col-md-12\" >\n            <h3 class=\"md-3 text-center\">Editar Usuário</h3>\n            <form #userForm=\"ngForm\">\n                <div *ngIf=\"userName.invalid && (userName.dirty || userName.touched)\" class=\"alert alert-danger\">\n                    <div *ngIf=\"userName.errors.required\">\n                        Preencher campo Nome do Usuário!\n                    </div>\n                    <div *ngIf=\"userName.errors.minlength\">\n                        O Nome do Usuário deve ter no mínimo 3 caracteres.\n                    </div>\n                </div><div *ngIf=\"password.invalid && (userName.dirty || password.touched)\" class=\"alert alert-danger\">\n                    <div *ngIf=\"password.errors.required\">\n                        Preencher campo Senha!\n                    </div>\n                    <div *ngIf=\"password.errors.minlength\">\n                        Sua senha deve ter no mínimo 8 caracteres.\n                    </div>\n                </div>          \n                <div class=\"form-group col-md-12\">\n                    <label for=\"userName\">Usuário:</label>\n                    <input id=\"userName\" type=\"text\" [(ngModel)]=\"user.nome\" name=\"userName\"\n                        class=\"form-control\" placeholder=\"Usuário\" required minlength=\"3\" #userName=\"ngModel\">\n                </div>\n                <div class=\"form-group col-md-12\">\n                    <label for=\"password\">Senha:</label>\n                    <input id=\"password\" type=\"password\" [(ngModel)]=\"user.senha\" name=\"password\"\n                        class=\"form-control\" placeholder=\"Senha\" required minlength=\"8\" #password=\"ngModel\">\n                </div>    \n                <div class=\"form-group col-md-12\">\n                   <button class=\"btn btn-success btn-lg\" (click)=\"updateUser()\" [disabled]=\"!userForm.form.valid\">Salvar</button>\n                </div>\n            </form>\n        </div>\n    </div>"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _template_usuario_usuario_create_usuario_create_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./template/usuario/usuario-create/usuario-create.component */ "./src/app/template/usuario/usuario-create/usuario-create.component.ts");
/* harmony import */ var _template_usuario_usuario_list_usuario_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./template/usuario/usuario-list/usuario-list.component */ "./src/app/template/usuario/usuario-list/usuario-list.component.ts");
/* harmony import */ var _template_usuario_usuario_update_usuario_update_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./template/usuario/usuario-update/usuario-update.component */ "./src/app/template/usuario/usuario-update/usuario-update.component.ts");
/* harmony import */ var _template_fuel_data_fuel_data_create_fuel_data_create_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./template/fuel-data/fuel-data-create/fuel-data-create.component */ "./src/app/template/fuel-data/fuel-data-create/fuel-data-create.component.ts");
/* harmony import */ var _template_fuel_data_fuel_data_list_fuel_data_list_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./template/fuel-data/fuel-data-list/fuel-data-list.component */ "./src/app/template/fuel-data/fuel-data-list/fuel-data-list.component.ts");
/* harmony import */ var _template_fuel_data_fuel_data_details_fuel_data_details_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./template/fuel-data/fuel-data-details/fuel-data-details.component */ "./src/app/template/fuel-data/fuel-data-details/fuel-data-details.component.ts");
/* harmony import */ var _template_fuel_data_fuel_data_update_fuel_data_update_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./template/fuel-data/fuel-data-update/fuel-data-update.component */ "./src/app/template/fuel-data/fuel-data-update/fuel-data-update.component.ts");
/* harmony import */ var _template_fuel_data_structure_list_by_region_by_region_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./template/fuel-data/structure-list/by-region/by-region.component */ "./src/app/template/fuel-data/structure-list/by-region/by-region.component.ts");
/* harmony import */ var _template_fuel_data_structure_list_by_revenda_by_revenda_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./template/fuel-data/structure-list/by-revenda/by-revenda.component */ "./src/app/template/fuel-data/structure-list/by-revenda/by-revenda.component.ts");
/* harmony import */ var _template_fuel_data_structure_list_by_coleta_by_coleta_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./template/fuel-data/structure-list/by-coleta/by-coleta.component */ "./src/app/template/fuel-data/structure-list/by-coleta/by-coleta.component.ts");
/* harmony import */ var _template_fuel_data_structure_list_media_municipio_media_municipio_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./template/fuel-data/structure-list/media-municipio/media-municipio.component */ "./src/app/template/fuel-data/structure-list/media-municipio/media-municipio.component.ts");
/* harmony import */ var _template_fuel_data_structure_list_media_bandeira_media_bandeira_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./template/fuel-data/structure-list/media-bandeira/media-bandeira.component */ "./src/app/template/fuel-data/structure-list/media-bandeira/media-bandeira.component.ts");















const routes = [
    { path: '', pathMatch: 'full', redirectTo: 'dadosabertos' },
    { path: 'novo-usuario', component: _template_usuario_usuario_create_usuario_create_component__WEBPACK_IMPORTED_MODULE_3__["UsuarioCreateComponent"] },
    { path: 'usuarios', component: _template_usuario_usuario_list_usuario_list_component__WEBPACK_IMPORTED_MODULE_4__["UsuarioListComponent"] },
    { path: 'usuario/:id', component: _template_usuario_usuario_update_usuario_update_component__WEBPACK_IMPORTED_MODULE_5__["UsuarioUpdateComponent"] },
    { path: 'novas-info', component: _template_fuel_data_fuel_data_create_fuel_data_create_component__WEBPACK_IMPORTED_MODULE_6__["FuelDataCreateComponent"] },
    { path: 'dadosabertos', component: _template_fuel_data_fuel_data_list_fuel_data_list_component__WEBPACK_IMPORTED_MODULE_7__["FuelDataListComponent"] },
    { path: 'dadosabertos/:id', component: _template_fuel_data_fuel_data_details_fuel_data_details_component__WEBPACK_IMPORTED_MODULE_8__["FuelDataDetailsComponent"] },
    { path: 'editar-info/:id', component: _template_fuel_data_fuel_data_update_fuel_data_update_component__WEBPACK_IMPORTED_MODULE_9__["FuelDataUpdateComponent"] },
    { path: 'regiao/:id', component: _template_fuel_data_structure_list_by_region_by_region_component__WEBPACK_IMPORTED_MODULE_10__["ByRegionComponent"] },
    { path: 'pordistribuidora', component: _template_fuel_data_structure_list_by_revenda_by_revenda_component__WEBPACK_IMPORTED_MODULE_11__["ByRevendaComponent"] },
    { path: 'datacoleta', component: _template_fuel_data_structure_list_by_coleta_by_coleta_component__WEBPACK_IMPORTED_MODULE_12__["ByColetaComponent"] },
    { path: 'media-municipio', component: _template_fuel_data_structure_list_media_municipio_media_municipio_component__WEBPACK_IMPORTED_MODULE_13__["MediaMunicipioComponent"] },
    { path: 'media-bandeira', component: _template_fuel_data_structure_list_media_bandeira_media_bandeira_component__WEBPACK_IMPORTED_MODULE_14__["MediaBandeiraComponent"] },
    { path: '**', component: _template_fuel_data_fuel_data_list_fuel_data_list_component__WEBPACK_IMPORTED_MODULE_7__["FuelDataListComponent"] }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'fuel-app';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm2015/ngx-bootstrap-modal.js");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/fesm2015/ng2-smart-table.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _template_usuario_usuario_create_usuario_create_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./template/usuario/usuario-create/usuario-create.component */ "./src/app/template/usuario/usuario-create/usuario-create.component.ts");
/* harmony import */ var _template_fuel_data_fuel_data_create_fuel_data_create_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./template/fuel-data/fuel-data-create/fuel-data-create.component */ "./src/app/template/fuel-data/fuel-data-create/fuel-data-create.component.ts");
/* harmony import */ var _template_usuario_usuario_list_usuario_list_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./template/usuario/usuario-list/usuario-list.component */ "./src/app/template/usuario/usuario-list/usuario-list.component.ts");
/* harmony import */ var _template_usuario_usuario_update_usuario_update_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./template/usuario/usuario-update/usuario-update.component */ "./src/app/template/usuario/usuario-update/usuario-update.component.ts");
/* harmony import */ var _template_fuel_data_fuel_data_list_fuel_data_list_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./template/fuel-data/fuel-data-list/fuel-data-list.component */ "./src/app/template/fuel-data/fuel-data-list/fuel-data-list.component.ts");
/* harmony import */ var jw_angular_pagination__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! jw-angular-pagination */ "./node_modules/jw-angular-pagination/lib/jw-pagination.component.js");
/* harmony import */ var jw_angular_pagination__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(jw_angular_pagination__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _template_fuel_data_fuel_data_update_fuel_data_update_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./template/fuel-data/fuel-data-update/fuel-data-update.component */ "./src/app/template/fuel-data/fuel-data-update/fuel-data-update.component.ts");
/* harmony import */ var _template_fuel_data_fuel_data_details_fuel_data_details_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./template/fuel-data/fuel-data-details/fuel-data-details.component */ "./src/app/template/fuel-data/fuel-data-details/fuel-data-details.component.ts");
/* harmony import */ var _template_fuel_data_structure_list_by_region_by_region_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./template/fuel-data/structure-list/by-region/by-region.component */ "./src/app/template/fuel-data/structure-list/by-region/by-region.component.ts");
/* harmony import */ var _template_fuel_data_structure_list_by_revenda_by_revenda_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./template/fuel-data/structure-list/by-revenda/by-revenda.component */ "./src/app/template/fuel-data/structure-list/by-revenda/by-revenda.component.ts");
/* harmony import */ var _template_fuel_data_structure_list_by_coleta_by_coleta_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./template/fuel-data/structure-list/by-coleta/by-coleta.component */ "./src/app/template/fuel-data/structure-list/by-coleta/by-coleta.component.ts");
/* harmony import */ var _template_fuel_data_structure_list_media_municipio_media_municipio_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./template/fuel-data/structure-list/media-municipio/media-municipio.component */ "./src/app/template/fuel-data/structure-list/media-municipio/media-municipio.component.ts");
/* harmony import */ var _template_fuel_data_structure_list_media_bandeira_media_bandeira_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./template/fuel-data/structure-list/media-bandeira/media-bandeira.component */ "./src/app/template/fuel-data/structure-list/media-bandeira/media-bandeira.component.ts");

























let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_10__["AppComponent"],
            _template_usuario_usuario_create_usuario_create_component__WEBPACK_IMPORTED_MODULE_11__["UsuarioCreateComponent"],
            _template_fuel_data_fuel_data_create_fuel_data_create_component__WEBPACK_IMPORTED_MODULE_12__["FuelDataCreateComponent"],
            _template_usuario_usuario_list_usuario_list_component__WEBPACK_IMPORTED_MODULE_13__["UsuarioListComponent"],
            _template_usuario_usuario_update_usuario_update_component__WEBPACK_IMPORTED_MODULE_14__["UsuarioUpdateComponent"],
            jw_angular_pagination__WEBPACK_IMPORTED_MODULE_16__["JwPaginationComponent"],
            _template_fuel_data_fuel_data_list_fuel_data_list_component__WEBPACK_IMPORTED_MODULE_15__["FuelDataListComponent"],
            _template_fuel_data_fuel_data_update_fuel_data_update_component__WEBPACK_IMPORTED_MODULE_17__["FuelDataUpdateComponent"],
            _template_fuel_data_fuel_data_details_fuel_data_details_component__WEBPACK_IMPORTED_MODULE_18__["FuelDataDetailsComponent"],
            _template_fuel_data_structure_list_by_region_by_region_component__WEBPACK_IMPORTED_MODULE_19__["ByRegionComponent"],
            _template_fuel_data_structure_list_by_revenda_by_revenda_component__WEBPACK_IMPORTED_MODULE_20__["ByRevendaComponent"],
            _template_fuel_data_structure_list_by_coleta_by_coleta_component__WEBPACK_IMPORTED_MODULE_21__["ByColetaComponent"],
            _template_fuel_data_structure_list_media_municipio_media_municipio_component__WEBPACK_IMPORTED_MODULE_22__["MediaMunicipioComponent"],
            _template_fuel_data_structure_list_media_bandeira_media_bandeira_component__WEBPACK_IMPORTED_MODULE_23__["MediaBandeiraComponent"]
        ],
        imports: [
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
            ng2_smart_table__WEBPACK_IMPORTED_MODULE_9__["Ng2SmartTableModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_8__["BrowserAnimationsModule"],
            ngx_bootstrap__WEBPACK_IMPORTED_MODULE_7__["BsDatepickerModule"].forRoot(),
            ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["ModalModule"].forRoot()
        ],
        providers: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_10__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/shared/rest-crud/fuel-data.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/shared/rest-crud/fuel-data.service.ts ***!
  \*******************************************************/
/*! exports provided: FuelDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FuelDataService", function() { return FuelDataService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");






let FuelDataService = class FuelDataService {
    constructor(http) {
        this.http = http;
        this.apiURL = "http://localhost:8080";
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"]({
                'Content-Type': 'application/json;'
            })
        };
    }
    getFuelData() {
        return this.http.get(this.apiURL + '/api/dadosabertos')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    getFuelDataById(id) {
        return this.http.get(this.apiURL + '/api/dadosabertos/' + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    createFuelData(data) {
        return this.http.post(this.apiURL + '/api/dadosabertos', JSON.stringify(data), this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    updateFuelData(id, data) {
        return this.http.put(this.apiURL + '/api/dadosabertos/' + id, JSON.stringify(data), this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    deleteFuelData(id) {
        return this.http.delete(this.apiURL + '/api/dadosabertos/' + id, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    getMediaMunicipio(municipio) {
        return this.http.get(this.apiURL + '/api/dadosabertos/mediaPrecos/' + municipio)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    getDataByRegion(regiao) {
        return this.http.get(this.apiURL + '/api/dadosabertos/regiao/' + regiao)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    getDataByRevenda() {
        return this.http.get(this.apiURL + '/api/dadosabertos/distribuidora')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    getDataByColeta() {
        return this.http.get(this.apiURL + '/api/dadosabertos/datacoleta')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    getMediaByMunicipios() {
        return this.http.get(this.apiURL + '/api/dadosabertos/mediaMunicipio')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    getMediaByBandeira() {
        return this.http.get(this.apiURL + '/api/dadosabertos/mediaBandeira')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    upload(data) {
        let uploadURL = this.apiURL + '/api/dadosabertos/upload';
        return this.http.post(uploadURL, data, {
            reportProgress: true,
            observe: 'events'
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])((event) => {
            switch (event.type) {
                case _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpEventType"].UploadProgress:
                    const progress = Math.round(100 * event.loaded / event.total);
                    return { status: 'progress', message: progress };
                case _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpEventType"].Response:
                    return event.body;
                default:
                    return `Unhandled event: ${event.type}`;
            }
        }));
    }
    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            errorMessage = error.error.message;
        }
        else {
            errorMessage = 'Error Code: ' + error.status + '\nMessage: ' + error.message;
        }
        window.alert(errorMessage);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(errorMessage);
    }
};
FuelDataService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] }
];
FuelDataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], FuelDataService);



/***/ }),

/***/ "./src/app/shared/rest-crud/usuario.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/shared/rest-crud/usuario.service.ts ***!
  \*****************************************************/
/*! exports provided: UsuarioService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuarioService", function() { return UsuarioService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");





let UsuarioService = class UsuarioService {
    constructor(http) {
        this.http = http;
        this.apiURL = "http://localhost:8080";
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"]({
                'Content-Type': 'application/json;'
            })
        };
    }
    getUsers() {
        return this.http.get(this.apiURL + '/api/usuarios')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    getUser(id) {
        return this.http.get(this.apiURL + '/api/usuarios/' + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    createUser(user) {
        return this.http.post(this.apiURL + '/api/usuarios', JSON.stringify(user), this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    updateUser(id, user) {
        return this.http.put(this.apiURL + '/api/usuarios/' + id, JSON.stringify(user), this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    deleteUser(id) {
        return this.http.delete(this.apiURL + '/api/usuarios/' + id, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            errorMessage = error.error.message;
        }
        else {
            errorMessage = 'Error Code: ' + error.status + '\nMessage: ' + error.message;
        }
        window.alert(errorMessage);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(errorMessage);
    }
};
UsuarioService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] }
];
UsuarioService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], UsuarioService);



/***/ }),

/***/ "./src/app/template/fuel-data/fuel-data-create/fuel-data-create.component.css":
/*!************************************************************************************!*\
  !*** ./src/app/template/fuel-data/fuel-data-create/fuel-data-create.component.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container{\r\n    width: 50%;\r\n}\r\n.btns{\r\n    margin: 10 10px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVtcGxhdGUvZnVlbC1kYXRhL2Z1ZWwtZGF0YS1jcmVhdGUvZnVlbC1kYXRhLWNyZWF0ZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksVUFBVTtBQUNkO0FBQ0E7SUFDSSxlQUFlO0FBQ25CIiwiZmlsZSI6InNyYy9hcHAvdGVtcGxhdGUvZnVlbC1kYXRhL2Z1ZWwtZGF0YS1jcmVhdGUvZnVlbC1kYXRhLWNyZWF0ZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lcntcclxuICAgIHdpZHRoOiA1MCU7XHJcbn1cclxuLmJ0bnN7XHJcbiAgICBtYXJnaW46IDEwIDEwcHg7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/template/fuel-data/fuel-data-create/fuel-data-create.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/template/fuel-data/fuel-data-create/fuel-data-create.component.ts ***!
  \***********************************************************************************/
/*! exports provided: FuelDataCreateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FuelDataCreateComponent", function() { return FuelDataCreateComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_shared_rest_crud_fuel_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/rest-crud/fuel-data.service */ "./src/app/shared/rest-crud/fuel-data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm2015/ngx-bootstrap-datepicker.js");
/* harmony import */ var ngx_bootstrap_chronos__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/chronos */ "./node_modules/ngx-bootstrap/chronos/fesm2015/ngx-bootstrap-chronos.js");
/* harmony import */ var ngx_bootstrap_locale__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/locale */ "./node_modules/ngx-bootstrap/locale/fesm2015/ngx-bootstrap-locale.js");







Object(ngx_bootstrap_chronos__WEBPACK_IMPORTED_MODULE_5__["defineLocale"])('pt-br', ngx_bootstrap_locale__WEBPACK_IMPORTED_MODULE_6__["ptBrLocale"]);
let FuelDataCreateComponent = class FuelDataCreateComponent {
    constructor(restApi, router, localeService) {
        this.restApi = restApi;
        this.router = router;
        this.localeService = localeService;
        this.fuelData = { medida: 'R$ / litro' };
        this.ufs = ['AC', 'AL', 'AM', 'AP', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MG', 'MS', 'MT', 'PA', 'PB', 'PE', 'PI', 'PR',
            'RJ', 'RN', 'RO', 'RR', 'RS', 'SC', 'SE', 'SP', 'TO'];
        this.produtos = ['DIESEL', 'DIESEL S10', 'ETANOL', 'GASOLINA'];
        this.localeService.use('pt-br');
        this.datePickerConfig = Object.assign({}, {
            containerClass: 'theme-dark-blue',
            showWeekNumbers: false,
            adaptivePosition: true
        });
    }
    ngOnInit() {
    }
    addFuelData() {
        this.restApi.createFuelData(this.fuelData).subscribe((data) => {
            this.router.navigate(['/dadosabertos']);
        });
    }
};
FuelDataCreateComponent.ctorParameters = () => [
    { type: src_app_shared_rest_crud_fuel_data_service__WEBPACK_IMPORTED_MODULE_2__["FuelDataService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_4__["BsLocaleService"] }
];
FuelDataCreateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-fuel-data-create',
        template: __webpack_require__(/*! raw-loader!./fuel-data-create.component.html */ "./node_modules/raw-loader/index.js!./src/app/template/fuel-data/fuel-data-create/fuel-data-create.component.html"),
        styles: [__webpack_require__(/*! ./fuel-data-create.component.css */ "./src/app/template/fuel-data/fuel-data-create/fuel-data-create.component.css")]
    })
], FuelDataCreateComponent);



/***/ }),

/***/ "./src/app/template/fuel-data/fuel-data-details/fuel-data-details.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/template/fuel-data/fuel-data-details/fuel-data-details.component.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "td, th{\r\n    text-align: center;\r\n}\r\n.container{\r\n    width: 50%;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVtcGxhdGUvZnVlbC1kYXRhL2Z1ZWwtZGF0YS1kZXRhaWxzL2Z1ZWwtZGF0YS1kZXRhaWxzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxrQkFBa0I7QUFDdEI7QUFDQTtJQUNJLFVBQVU7QUFDZCIsImZpbGUiOiJzcmMvYXBwL3RlbXBsYXRlL2Z1ZWwtZGF0YS9mdWVsLWRhdGEtZGV0YWlscy9mdWVsLWRhdGEtZGV0YWlscy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGQsIHRoe1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi5jb250YWluZXJ7XHJcbiAgICB3aWR0aDogNTAlO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/template/fuel-data/fuel-data-details/fuel-data-details.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/template/fuel-data/fuel-data-details/fuel-data-details.component.ts ***!
  \*************************************************************************************/
/*! exports provided: FuelDataDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FuelDataDetailsComponent", function() { return FuelDataDetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_shared_rest_crud_fuel_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/rest-crud/fuel-data.service */ "./src/app/shared/rest-crud/fuel-data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let FuelDataDetailsComponent = class FuelDataDetailsComponent {
    constructor(apiRest, actRoute, router) {
        this.apiRest = apiRest;
        this.actRoute = actRoute;
        this.router = router;
        this.id = this.actRoute.snapshot.params['id'];
        this.data = {};
    }
    ngOnInit() {
        this.getDataById();
    }
    getDataById() {
        this.apiRest.getFuelDataById(this.id).subscribe((data) => {
            this.data = data;
        });
    }
    deleteData(id) {
        if (window.confirm("Você tem certeza que deseja deletar essas informações!?")) {
            this.apiRest.deleteFuelData(id).subscribe(data => {
                this.router.navigate(['/dadosabertos']);
            });
        }
    }
};
FuelDataDetailsComponent.ctorParameters = () => [
    { type: src_app_shared_rest_crud_fuel_data_service__WEBPACK_IMPORTED_MODULE_2__["FuelDataService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
FuelDataDetailsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-fuel-data-details',
        template: __webpack_require__(/*! raw-loader!./fuel-data-details.component.html */ "./node_modules/raw-loader/index.js!./src/app/template/fuel-data/fuel-data-details/fuel-data-details.component.html"),
        styles: [__webpack_require__(/*! ./fuel-data-details.component.css */ "./src/app/template/fuel-data/fuel-data-details/fuel-data-details.component.css")]
    })
], FuelDataDetailsComponent);



/***/ }),

/***/ "./src/app/template/fuel-data/fuel-data-list/fuel-data-list.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/template/fuel-data/fuel-data-list/fuel-data-list.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container{\r\n    width: 100%\r\n}\r\nth, td{\r\n    text-align: center;\r\n}\r\n.last{\r\n    width: 20%;\r\n    text-align: right;\r\n}\r\n.last button{\r\n    margin:5px\r\n}\r\n.fileup{\r\n    text-align: center;\r\n}\r\n.fileup input{\r\n    margin: 0 auto;\r\n    width: 40%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVtcGxhdGUvZnVlbC1kYXRhL2Z1ZWwtZGF0YS1saXN0L2Z1ZWwtZGF0YS1saXN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSTtBQUNKO0FBQ0E7SUFDSSxrQkFBa0I7QUFDdEI7QUFDQTtJQUNJLFVBQVU7SUFDVixpQkFBaUI7QUFDckI7QUFDQTtJQUNJO0FBQ0o7QUFDQTtJQUNJLGtCQUFrQjtBQUN0QjtBQUNBO0lBQ0ksY0FBYztJQUNkLFVBQVU7QUFDZCIsImZpbGUiOiJzcmMvYXBwL3RlbXBsYXRlL2Z1ZWwtZGF0YS9mdWVsLWRhdGEtbGlzdC9mdWVsLWRhdGEtbGlzdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lcntcclxuICAgIHdpZHRoOiAxMDAlXHJcbn1cclxudGgsIHRke1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi5sYXN0e1xyXG4gICAgd2lkdGg6IDIwJTtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcbi5sYXN0IGJ1dHRvbntcclxuICAgIG1hcmdpbjo1cHhcclxufVxyXG4uZmlsZXVwe1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi5maWxldXAgaW5wdXR7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIHdpZHRoOiA0MCU7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/template/fuel-data/fuel-data-list/fuel-data-list.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/template/fuel-data/fuel-data-list/fuel-data-list.component.ts ***!
  \*******************************************************************************/
/*! exports provided: FuelDataListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FuelDataListComponent", function() { return FuelDataListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_shared_rest_crud_fuel_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/rest-crud/fuel-data.service */ "./src/app/shared/rest-crud/fuel-data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");





let FuelDataListComponent = class FuelDataListComponent {
    constructor(restApi, router, formBuilder) {
        this.restApi = restApi;
        this.router = router;
        this.formBuilder = formBuilder;
        this.fuelDataResult = {};
        this.fuelData = { municipio: '', regiao: '', revenda: '' };
        this.uploadResponse = { status: '', message: '', filePath: '' };
    }
    ngOnInit() {
        this.loadFuelData();
        this.form = this.formBuilder.group({ fileCsv: [''] });
    }
    onFileChange(event) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            this.form.get('fileCsv').setValue(file);
        }
    }
    onSubmit() {
        const formData = new FormData();
        formData.append('file', this.form.get('fileCsv').value);
        this.restApi.upload(formData).subscribe((res) => this.uploadResponse = res, (err) => this.error = err);
        window.alert("Em alguns instantes as informações de sua planilha serão salvas no nosso Banco!");
    }
    onChangePage(pageOfItems) {
        this.pageOfItems = pageOfItems;
    }
    loadFuelData() {
        return this.restApi.getFuelData().subscribe((data) => {
            this.fuels = data;
        });
    }
    deleteData(id) {
        if (window.confirm("Você tem certeza que deseja deletar esta informação!?")) {
            this.restApi.deleteFuelData(id).subscribe(data => {
                this.loadFuelData();
            });
        }
    }
    searchData() {
        return this.restApi.getMediaMunicipio(this.fuelData.municipio).subscribe(data => {
            this.fuelDataResult = data;
        });
    }
};
FuelDataListComponent.ctorParameters = () => [
    { type: src_app_shared_rest_crud_fuel_data_service__WEBPACK_IMPORTED_MODULE_2__["FuelDataService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] }
];
FuelDataListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-fuel-data-list',
        template: __webpack_require__(/*! raw-loader!./fuel-data-list.component.html */ "./node_modules/raw-loader/index.js!./src/app/template/fuel-data/fuel-data-list/fuel-data-list.component.html"),
        styles: [__webpack_require__(/*! ./fuel-data-list.component.css */ "./src/app/template/fuel-data/fuel-data-list/fuel-data-list.component.css")]
    })
], FuelDataListComponent);



/***/ }),

/***/ "./src/app/template/fuel-data/fuel-data-update/fuel-data-update.component.css":
/*!************************************************************************************!*\
  !*** ./src/app/template/fuel-data/fuel-data-update/fuel-data-update.component.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container{\r\n    width: 50%;\r\n}\r\n.btns{\r\n    margin: 10 10px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVtcGxhdGUvZnVlbC1kYXRhL2Z1ZWwtZGF0YS11cGRhdGUvZnVlbC1kYXRhLXVwZGF0ZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksVUFBVTtBQUNkO0FBQ0E7SUFDSSxlQUFlO0FBQ25CIiwiZmlsZSI6InNyYy9hcHAvdGVtcGxhdGUvZnVlbC1kYXRhL2Z1ZWwtZGF0YS11cGRhdGUvZnVlbC1kYXRhLXVwZGF0ZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lcntcclxuICAgIHdpZHRoOiA1MCU7XHJcbn1cclxuLmJ0bnN7XHJcbiAgICBtYXJnaW46IDEwIDEwcHg7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/template/fuel-data/fuel-data-update/fuel-data-update.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/template/fuel-data/fuel-data-update/fuel-data-update.component.ts ***!
  \***********************************************************************************/
/*! exports provided: FuelDataUpdateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FuelDataUpdateComponent", function() { return FuelDataUpdateComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_shared_rest_crud_fuel_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/rest-crud/fuel-data.service */ "./src/app/shared/rest-crud/fuel-data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let FuelDataUpdateComponent = class FuelDataUpdateComponent {
    constructor(restApi, actRoute, router) {
        this.restApi = restApi;
        this.actRoute = actRoute;
        this.router = router;
        this.id = this.actRoute.snapshot.params['id'];
        this.fuelData = {};
        this.ufs = ['AC', 'AL', 'AM', 'AP', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MG', 'MS', 'MT', 'PA', 'PB', 'PE', 'PI', 'PR',
            'RJ', 'RN', 'RO', 'RR', 'RS', 'SC', 'SE', 'SP', 'TO'];
        this.produtos = ['DIESEL', 'DIESEL S10', 'ETANOL', 'GASOLINA'];
    }
    ngOnInit() {
        this.getFuelData();
    }
    getFuelData() {
        this.restApi.getFuelDataById(this.id).subscribe((data) => {
            this.fuelData = data;
        });
    }
    updateFuelData() {
        if (window.confirm('Certeza que deseja alterar essas Informações?')) {
            this.restApi.updateFuelData(this.id, this.fuelData).subscribe(data => {
                this.router.navigate(['/dadosabertos']);
            });
        }
    }
};
FuelDataUpdateComponent.ctorParameters = () => [
    { type: src_app_shared_rest_crud_fuel_data_service__WEBPACK_IMPORTED_MODULE_2__["FuelDataService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
FuelDataUpdateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-fuel-data-update',
        template: __webpack_require__(/*! raw-loader!./fuel-data-update.component.html */ "./node_modules/raw-loader/index.js!./src/app/template/fuel-data/fuel-data-update/fuel-data-update.component.html"),
        styles: [__webpack_require__(/*! ./fuel-data-update.component.css */ "./src/app/template/fuel-data/fuel-data-update/fuel-data-update.component.css")]
    })
], FuelDataUpdateComponent);



/***/ }),

/***/ "./src/app/template/fuel-data/structure-list/by-coleta/by-coleta.component.css":
/*!*************************************************************************************!*\
  !*** ./src/app/template/fuel-data/structure-list/by-coleta/by-coleta.component.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container{\r\n    width: 100%\r\n}\r\nth, td{\r\n    text-align: center;\r\n}\r\n.last{\r\n    width: 20%;\r\n    text-align: right;\r\n}\r\n.last button{\r\n    margin:5px\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVtcGxhdGUvZnVlbC1kYXRhL3N0cnVjdHVyZS1saXN0L2J5LWNvbGV0YS9ieS1jb2xldGEuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJO0FBQ0o7QUFDQTtJQUNJLGtCQUFrQjtBQUN0QjtBQUNBO0lBQ0ksVUFBVTtJQUNWLGlCQUFpQjtBQUNyQjtBQUNBO0lBQ0k7QUFDSiIsImZpbGUiOiJzcmMvYXBwL3RlbXBsYXRlL2Z1ZWwtZGF0YS9zdHJ1Y3R1cmUtbGlzdC9ieS1jb2xldGEvYnktY29sZXRhLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGFpbmVye1xyXG4gICAgd2lkdGg6IDEwMCVcclxufVxyXG50aCwgdGR7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLmxhc3R7XHJcbiAgICB3aWR0aDogMjAlO1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbn1cclxuLmxhc3QgYnV0dG9ue1xyXG4gICAgbWFyZ2luOjVweFxyXG59Il19 */"

/***/ }),

/***/ "./src/app/template/fuel-data/structure-list/by-coleta/by-coleta.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/template/fuel-data/structure-list/by-coleta/by-coleta.component.ts ***!
  \************************************************************************************/
/*! exports provided: ByColetaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ByColetaComponent", function() { return ByColetaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_shared_rest_crud_fuel_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/rest-crud/fuel-data.service */ "./src/app/shared/rest-crud/fuel-data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let ByColetaComponent = class ByColetaComponent {
    constructor(restApi, router) {
        this.restApi = restApi;
        this.router = router;
        this.fuelData = {
            municipio: '',
            regiao: '',
            revenda: ''
        };
        this.fuelDataResult = {};
    }
    ngOnInit() {
        this.loadFuelData();
    }
    onChangePage(pageOfItems) {
        // update current page of items
        this.pageOfItems = pageOfItems;
    }
    loadFuelData() {
        return this.restApi.getDataByColeta().subscribe((data) => {
            this.fuels = data;
        });
    }
    deleteData(id) {
        if (window.confirm("Você tem certeza que deseja deletar essas informações!?")) {
            this.restApi.deleteFuelData(id).subscribe(data => {
                this.loadFuelData();
            });
        }
    }
    searchData() {
        return this.restApi.getMediaMunicipio(this.fuelData.municipio).subscribe(data => {
            this.fuelDataResult = data;
        });
    }
};
ByColetaComponent.ctorParameters = () => [
    { type: src_app_shared_rest_crud_fuel_data_service__WEBPACK_IMPORTED_MODULE_2__["FuelDataService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
ByColetaComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-by-coleta',
        template: __webpack_require__(/*! raw-loader!./by-coleta.component.html */ "./node_modules/raw-loader/index.js!./src/app/template/fuel-data/structure-list/by-coleta/by-coleta.component.html"),
        styles: [__webpack_require__(/*! ./by-coleta.component.css */ "./src/app/template/fuel-data/structure-list/by-coleta/by-coleta.component.css")]
    })
], ByColetaComponent);



/***/ }),

/***/ "./src/app/template/fuel-data/structure-list/by-region/by-region.component.css":
/*!*************************************************************************************!*\
  !*** ./src/app/template/fuel-data/structure-list/by-region/by-region.component.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container{\r\n    width: 100%\r\n}\r\nth, td{\r\n    text-align: center;\r\n}\r\n.last{\r\n    width: 20%;\r\n    text-align: right;\r\n}\r\n.last button{\r\n    margin:5px\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVtcGxhdGUvZnVlbC1kYXRhL3N0cnVjdHVyZS1saXN0L2J5LXJlZ2lvbi9ieS1yZWdpb24uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJO0FBQ0o7QUFDQTtJQUNJLGtCQUFrQjtBQUN0QjtBQUNBO0lBQ0ksVUFBVTtJQUNWLGlCQUFpQjtBQUNyQjtBQUNBO0lBQ0k7QUFDSiIsImZpbGUiOiJzcmMvYXBwL3RlbXBsYXRlL2Z1ZWwtZGF0YS9zdHJ1Y3R1cmUtbGlzdC9ieS1yZWdpb24vYnktcmVnaW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGFpbmVye1xyXG4gICAgd2lkdGg6IDEwMCVcclxufVxyXG50aCwgdGR7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLmxhc3R7XHJcbiAgICB3aWR0aDogMjAlO1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbn1cclxuLmxhc3QgYnV0dG9ue1xyXG4gICAgbWFyZ2luOjVweFxyXG59Il19 */"

/***/ }),

/***/ "./src/app/template/fuel-data/structure-list/by-region/by-region.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/template/fuel-data/structure-list/by-region/by-region.component.ts ***!
  \************************************************************************************/
/*! exports provided: ByRegionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ByRegionComponent", function() { return ByRegionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_shared_rest_crud_fuel_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/rest-crud/fuel-data.service */ "./src/app/shared/rest-crud/fuel-data.service.ts");




let ByRegionComponent = class ByRegionComponent {
    constructor(apiRest, actRoute, router) {
        this.apiRest = apiRest;
        this.actRoute = actRoute;
        this.router = router;
        this.regiao = this.actRoute.snapshot.params['id'];
        this.fuelDataResult = {};
        this.fuelData = {
            municipio: '',
            regiao: ''
        };
    }
    ngOnInit() {
        this.getDataByRegion();
    }
    onChangePage(pageOfItems) {
        // update current page of items
        this.pageOfItems = pageOfItems;
    }
    getDataByRegion() {
        this.apiRest.getDataByRegion(this.regiao).subscribe((data) => {
            this.data = data;
            this.titulo = this.data[0].regiao;
        });
    }
    deleteData(id) {
        if (window.confirm("Você tem certeza que deseja deletar essas informações!?")) {
            this.apiRest.deleteFuelData(id).subscribe(data => {
                this.ngOnInit();
            });
        }
    }
    searchWithReload(regiao) {
        this.apiRest.getDataByRegion(regiao).subscribe((data) => {
            this.data = data;
        });
    }
    searchMediaData() {
        return this.apiRest.getMediaMunicipio(this.fuelData.municipio).subscribe(data => {
            this.fuelDataResult = data;
        });
    }
};
ByRegionComponent.ctorParameters = () => [
    { type: src_app_shared_rest_crud_fuel_data_service__WEBPACK_IMPORTED_MODULE_3__["FuelDataService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
ByRegionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-by-region',
        template: __webpack_require__(/*! raw-loader!./by-region.component.html */ "./node_modules/raw-loader/index.js!./src/app/template/fuel-data/structure-list/by-region/by-region.component.html"),
        styles: [__webpack_require__(/*! ./by-region.component.css */ "./src/app/template/fuel-data/structure-list/by-region/by-region.component.css")]
    })
], ByRegionComponent);



/***/ }),

/***/ "./src/app/template/fuel-data/structure-list/by-revenda/by-revenda.component.css":
/*!***************************************************************************************!*\
  !*** ./src/app/template/fuel-data/structure-list/by-revenda/by-revenda.component.css ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container{\r\n    width: 100%\r\n}\r\nth, td{\r\n    text-align: center;\r\n}\r\n.last{\r\n    width: 20%;\r\n    text-align: right;\r\n}\r\n.last button{\r\n    margin:5px\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVtcGxhdGUvZnVlbC1kYXRhL3N0cnVjdHVyZS1saXN0L2J5LXJldmVuZGEvYnktcmV2ZW5kYS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0k7QUFDSjtBQUNBO0lBQ0ksa0JBQWtCO0FBQ3RCO0FBQ0E7SUFDSSxVQUFVO0lBQ1YsaUJBQWlCO0FBQ3JCO0FBQ0E7SUFDSTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvdGVtcGxhdGUvZnVlbC1kYXRhL3N0cnVjdHVyZS1saXN0L2J5LXJldmVuZGEvYnktcmV2ZW5kYS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lcntcclxuICAgIHdpZHRoOiAxMDAlXHJcbn1cclxudGgsIHRke1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi5sYXN0e1xyXG4gICAgd2lkdGg6IDIwJTtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcbi5sYXN0IGJ1dHRvbntcclxuICAgIG1hcmdpbjo1cHhcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/template/fuel-data/structure-list/by-revenda/by-revenda.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/template/fuel-data/structure-list/by-revenda/by-revenda.component.ts ***!
  \**************************************************************************************/
/*! exports provided: ByRevendaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ByRevendaComponent", function() { return ByRevendaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_app_shared_rest_crud_fuel_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/rest-crud/fuel-data.service */ "./src/app/shared/rest-crud/fuel-data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let ByRevendaComponent = class ByRevendaComponent {
    constructor(restApi, router, formBuilder) {
        this.restApi = restApi;
        this.router = router;
        this.formBuilder = formBuilder;
        this.fuelData = {
            municipio: '',
            regiao: '',
            revenda: ''
        };
        this.fuelDataResult = {};
    }
    ngOnInit() {
        this.loadFuelData();
    }
    onChangePage(pageOfItems) {
        // update current page of items
        this.pageOfItems = pageOfItems;
    }
    loadFuelData() {
        return this.restApi.getDataByRevenda().subscribe((data) => {
            this.fuels = data;
        });
    }
    deleteData(id) {
        if (window.confirm("Você tem certeza que deseja deletar essas informações!?")) {
            this.restApi.deleteFuelData(id).subscribe(data => {
                this.loadFuelData();
            });
        }
    }
    searchData() {
        return this.restApi.getMediaMunicipio(this.fuelData.municipio).subscribe(data => {
            this.fuelDataResult = data;
        });
    }
};
ByRevendaComponent.ctorParameters = () => [
    { type: src_app_shared_rest_crud_fuel_data_service__WEBPACK_IMPORTED_MODULE_3__["FuelDataService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] }
];
ByRevendaComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-by-revenda',
        template: __webpack_require__(/*! raw-loader!./by-revenda.component.html */ "./node_modules/raw-loader/index.js!./src/app/template/fuel-data/structure-list/by-revenda/by-revenda.component.html"),
        styles: [__webpack_require__(/*! ./by-revenda.component.css */ "./src/app/template/fuel-data/structure-list/by-revenda/by-revenda.component.css")]
    })
], ByRevendaComponent);



/***/ }),

/***/ "./src/app/template/fuel-data/structure-list/media-bandeira/media-bandeira.component.css":
/*!***********************************************************************************************!*\
  !*** ./src/app/template/fuel-data/structure-list/media-bandeira/media-bandeira.component.css ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container{\r\n    width: 100%\r\n}\r\nth, td{\r\n    text-align: center;\r\n}\r\n.last{\r\n    width: 20%;\r\n    text-align: right;\r\n}\r\n.last button{\r\n    margin:5px\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVtcGxhdGUvZnVlbC1kYXRhL3N0cnVjdHVyZS1saXN0L21lZGlhLWJhbmRlaXJhL21lZGlhLWJhbmRlaXJhLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSTtBQUNKO0FBQ0E7SUFDSSxrQkFBa0I7QUFDdEI7QUFDQTtJQUNJLFVBQVU7SUFDVixpQkFBaUI7QUFDckI7QUFDQTtJQUNJO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC90ZW1wbGF0ZS9mdWVsLWRhdGEvc3RydWN0dXJlLWxpc3QvbWVkaWEtYmFuZGVpcmEvbWVkaWEtYmFuZGVpcmEuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXJ7XHJcbiAgICB3aWR0aDogMTAwJVxyXG59XHJcbnRoLCB0ZHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4ubGFzdHtcclxuICAgIHdpZHRoOiAyMCU7XHJcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcclxufVxyXG4ubGFzdCBidXR0b257XHJcbiAgICBtYXJnaW46NXB4XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/template/fuel-data/structure-list/media-bandeira/media-bandeira.component.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/template/fuel-data/structure-list/media-bandeira/media-bandeira.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: MediaBandeiraComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MediaBandeiraComponent", function() { return MediaBandeiraComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_shared_rest_crud_fuel_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/rest-crud/fuel-data.service */ "./src/app/shared/rest-crud/fuel-data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let MediaBandeiraComponent = class MediaBandeiraComponent {
    constructor(restApi, router) {
        this.restApi = restApi;
        this.router = router;
        this.fuelData = {
            municipio: '',
            regiao: '',
            revenda: ''
        };
        this.fuelDataResult = {};
    }
    ngOnInit() {
        this.loadFuelData();
    }
    onChangePage(pageOfItems) {
        // update current page of items
        this.pageOfItems = pageOfItems;
    }
    loadFuelData() {
        return this.restApi.getMediaByBandeira().subscribe((data) => {
            this.fuels = data;
        });
    }
    searchData() {
        return this.restApi.getMediaMunicipio(this.fuelData.municipio).subscribe(data => {
            this.fuelDataResult = data;
        });
    }
};
MediaBandeiraComponent.ctorParameters = () => [
    { type: src_app_shared_rest_crud_fuel_data_service__WEBPACK_IMPORTED_MODULE_2__["FuelDataService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
MediaBandeiraComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-media-bandeira',
        template: __webpack_require__(/*! raw-loader!./media-bandeira.component.html */ "./node_modules/raw-loader/index.js!./src/app/template/fuel-data/structure-list/media-bandeira/media-bandeira.component.html"),
        styles: [__webpack_require__(/*! ./media-bandeira.component.css */ "./src/app/template/fuel-data/structure-list/media-bandeira/media-bandeira.component.css")]
    })
], MediaBandeiraComponent);



/***/ }),

/***/ "./src/app/template/fuel-data/structure-list/media-municipio/media-municipio.component.css":
/*!*************************************************************************************************!*\
  !*** ./src/app/template/fuel-data/structure-list/media-municipio/media-municipio.component.css ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container{\r\n    width: 100%\r\n}\r\nth, td{\r\n    text-align: center;\r\n}\r\n.last{\r\n    width: 20%;\r\n    text-align: right;\r\n}\r\n.last button{\r\n    margin:5px\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVtcGxhdGUvZnVlbC1kYXRhL3N0cnVjdHVyZS1saXN0L21lZGlhLW11bmljaXBpby9tZWRpYS1tdW5pY2lwaW8uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJO0FBQ0o7QUFDQTtJQUNJLGtCQUFrQjtBQUN0QjtBQUNBO0lBQ0ksVUFBVTtJQUNWLGlCQUFpQjtBQUNyQjtBQUNBO0lBQ0k7QUFDSiIsImZpbGUiOiJzcmMvYXBwL3RlbXBsYXRlL2Z1ZWwtZGF0YS9zdHJ1Y3R1cmUtbGlzdC9tZWRpYS1tdW5pY2lwaW8vbWVkaWEtbXVuaWNpcGlvLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGFpbmVye1xyXG4gICAgd2lkdGg6IDEwMCVcclxufVxyXG50aCwgdGR7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLmxhc3R7XHJcbiAgICB3aWR0aDogMjAlO1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbn1cclxuLmxhc3QgYnV0dG9ue1xyXG4gICAgbWFyZ2luOjVweFxyXG59Il19 */"

/***/ }),

/***/ "./src/app/template/fuel-data/structure-list/media-municipio/media-municipio.component.ts":
/*!************************************************************************************************!*\
  !*** ./src/app/template/fuel-data/structure-list/media-municipio/media-municipio.component.ts ***!
  \************************************************************************************************/
/*! exports provided: MediaMunicipioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MediaMunicipioComponent", function() { return MediaMunicipioComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_shared_rest_crud_fuel_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/rest-crud/fuel-data.service */ "./src/app/shared/rest-crud/fuel-data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let MediaMunicipioComponent = class MediaMunicipioComponent {
    constructor(restApi, router) {
        this.restApi = restApi;
        this.router = router;
        this.fuelData = {
            municipio: '',
            regiao: '',
            revenda: ''
        };
        this.fuelDataResult = {};
    }
    ngOnInit() {
        this.loadFuelData();
    }
    onChangePage(pageOfItems) {
        // update current page of items
        this.pageOfItems = pageOfItems;
    }
    loadFuelData() {
        return this.restApi.getMediaByMunicipios().subscribe((data) => {
            this.fuels = data;
        });
    }
    searchData() {
        return this.restApi.getMediaMunicipio(this.fuelData.municipio).subscribe(data => {
            this.fuelDataResult = data;
        });
    }
};
MediaMunicipioComponent.ctorParameters = () => [
    { type: src_app_shared_rest_crud_fuel_data_service__WEBPACK_IMPORTED_MODULE_2__["FuelDataService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
MediaMunicipioComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-media-municipio',
        template: __webpack_require__(/*! raw-loader!./media-municipio.component.html */ "./node_modules/raw-loader/index.js!./src/app/template/fuel-data/structure-list/media-municipio/media-municipio.component.html"),
        styles: [__webpack_require__(/*! ./media-municipio.component.css */ "./src/app/template/fuel-data/structure-list/media-municipio/media-municipio.component.css")]
    })
], MediaMunicipioComponent);



/***/ }),

/***/ "./src/app/template/usuario/usuario-create/usuario-create.component.css":
/*!******************************************************************************!*\
  !*** ./src/app/template/usuario/usuario-create/usuario-create.component.css ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container{\r\n    width: 50%;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVtcGxhdGUvdXN1YXJpby91c3VhcmlvLWNyZWF0ZS91c3VhcmlvLWNyZWF0ZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksVUFBVTtBQUNkIiwiZmlsZSI6InNyYy9hcHAvdGVtcGxhdGUvdXN1YXJpby91c3VhcmlvLWNyZWF0ZS91c3VhcmlvLWNyZWF0ZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lcntcclxuICAgIHdpZHRoOiA1MCU7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/template/usuario/usuario-create/usuario-create.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/template/usuario/usuario-create/usuario-create.component.ts ***!
  \*****************************************************************************/
/*! exports provided: UsuarioCreateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuarioCreateComponent", function() { return UsuarioCreateComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_shared_rest_crud_usuario_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/rest-crud/usuario.service */ "./src/app/shared/rest-crud/usuario.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let UsuarioCreateComponent = class UsuarioCreateComponent {
    constructor(restApi, router) {
        this.restApi = restApi;
        this.router = router;
        this.user = {
            nome: '',
            senha: ''
        };
    }
    ngOnInit() {
    }
    addUser() {
        this.restApi.createUser(this.user).subscribe((data) => {
            this.router.navigate(['/usuarios']);
        });
    }
};
UsuarioCreateComponent.ctorParameters = () => [
    { type: src_app_shared_rest_crud_usuario_service__WEBPACK_IMPORTED_MODULE_2__["UsuarioService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], UsuarioCreateComponent.prototype, "user", void 0);
UsuarioCreateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-usuario-create',
        template: __webpack_require__(/*! raw-loader!./usuario-create.component.html */ "./node_modules/raw-loader/index.js!./src/app/template/usuario/usuario-create/usuario-create.component.html"),
        styles: [__webpack_require__(/*! ./usuario-create.component.css */ "./src/app/template/usuario/usuario-create/usuario-create.component.css")]
    })
], UsuarioCreateComponent);



/***/ }),

/***/ "./src/app/template/usuario/usuario-list/usuario-list.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/template/usuario/usuario-list/usuario-list.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container{\r\n    width: 50%;\r\n}\r\nth, td{\r\n    text-align: center;\r\n}\r\n.last{\r\n    width: 35%;\r\n    text-align: right;\r\n}\r\n.last button{\r\n    margin:5px\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVtcGxhdGUvdXN1YXJpby91c3VhcmlvLWxpc3QvdXN1YXJpby1saXN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxVQUFVO0FBQ2Q7QUFDQTtJQUNJLGtCQUFrQjtBQUN0QjtBQUNBO0lBQ0ksVUFBVTtJQUNWLGlCQUFpQjtBQUNyQjtBQUNBO0lBQ0k7QUFDSiIsImZpbGUiOiJzcmMvYXBwL3RlbXBsYXRlL3VzdWFyaW8vdXN1YXJpby1saXN0L3VzdWFyaW8tbGlzdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lcntcclxuICAgIHdpZHRoOiA1MCU7XHJcbn1cclxudGgsIHRke1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi5sYXN0e1xyXG4gICAgd2lkdGg6IDM1JTtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcbi5sYXN0IGJ1dHRvbntcclxuICAgIG1hcmdpbjo1cHhcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/template/usuario/usuario-list/usuario-list.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/template/usuario/usuario-list/usuario-list.component.ts ***!
  \*************************************************************************/
/*! exports provided: UsuarioListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuarioListComponent", function() { return UsuarioListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_shared_rest_crud_usuario_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/rest-crud/usuario.service */ "./src/app/shared/rest-crud/usuario.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let UsuarioListComponent = class UsuarioListComponent {
    constructor(restApi, router) {
        this.restApi = restApi;
        this.router = router;
        this.users = [];
    }
    ngOnInit() {
        this.loadUsers();
    }
    loadUsers() {
        return this.restApi.getUsers().subscribe((data) => {
            this.users = data;
        });
    }
    deleteUser(id) {
        if (window.confirm("Você tem certeza que deseja deletar este Usuário!?")) {
            this.restApi.deleteUser(id).subscribe(data => {
                this.loadUsers();
            });
        }
    }
};
UsuarioListComponent.ctorParameters = () => [
    { type: src_app_shared_rest_crud_usuario_service__WEBPACK_IMPORTED_MODULE_2__["UsuarioService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
UsuarioListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-usuario-list',
        template: __webpack_require__(/*! raw-loader!./usuario-list.component.html */ "./node_modules/raw-loader/index.js!./src/app/template/usuario/usuario-list/usuario-list.component.html"),
        styles: [__webpack_require__(/*! ./usuario-list.component.css */ "./src/app/template/usuario/usuario-list/usuario-list.component.css")]
    })
], UsuarioListComponent);



/***/ }),

/***/ "./src/app/template/usuario/usuario-update/usuario-update.component.css":
/*!******************************************************************************!*\
  !*** ./src/app/template/usuario/usuario-update/usuario-update.component.css ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container{\r\n    width: 50%;\r\n}\r\nth, td{\r\n    text-align: center;\r\n}\r\n.last{\r\n    width: 35%;\r\n    text-align: right;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVtcGxhdGUvdXN1YXJpby91c3VhcmlvLXVwZGF0ZS91c3VhcmlvLXVwZGF0ZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksVUFBVTtBQUNkO0FBQ0E7SUFDSSxrQkFBa0I7QUFDdEI7QUFDQTtJQUNJLFVBQVU7SUFDVixpQkFBaUI7QUFDckIiLCJmaWxlIjoic3JjL2FwcC90ZW1wbGF0ZS91c3VhcmlvL3VzdWFyaW8tdXBkYXRlL3VzdWFyaW8tdXBkYXRlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGFpbmVye1xyXG4gICAgd2lkdGg6IDUwJTtcclxufVxyXG50aCwgdGR7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLmxhc3R7XHJcbiAgICB3aWR0aDogMzUlO1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/template/usuario/usuario-update/usuario-update.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/template/usuario/usuario-update/usuario-update.component.ts ***!
  \*****************************************************************************/
/*! exports provided: UsuarioUpdateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuarioUpdateComponent", function() { return UsuarioUpdateComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_shared_rest_crud_usuario_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/rest-crud/usuario.service */ "./src/app/shared/rest-crud/usuario.service.ts");




let UsuarioUpdateComponent = class UsuarioUpdateComponent {
    constructor(restApi, actRoute, router) {
        this.restApi = restApi;
        this.actRoute = actRoute;
        this.router = router;
        this.id = this.actRoute.snapshot.params['id'];
        this.user = {};
    }
    ngOnInit() {
        this.getUser();
    }
    getUser() {
        this.restApi.getUser(this.id).subscribe((data) => {
            this.user = data;
        });
    }
    updateUser() {
        if (window.confirm('Certeza que deseja alterar dados do Usuário?')) {
            this.restApi.updateUser(this.id, this.user).subscribe(data => {
                this.router.navigate(['/usuarios']);
            });
        }
    }
};
UsuarioUpdateComponent.ctorParameters = () => [
    { type: src_app_shared_rest_crud_usuario_service__WEBPACK_IMPORTED_MODULE_3__["UsuarioService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
UsuarioUpdateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-usuario-update',
        template: __webpack_require__(/*! raw-loader!./usuario-update.component.html */ "./node_modules/raw-loader/index.js!./src/app/template/usuario/usuario-update/usuario-update.component.html"),
        styles: [__webpack_require__(/*! ./usuario-update.component.css */ "./src/app/template/usuario/usuario-update/usuario-update.component.css")]
    })
], UsuarioUpdateComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\souto\Documents\workspace-sts\fuel-data\fuel-app\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map